## Getting the app ready for the app store
Run the following scripts in order:
1) `"build:release": "ionic cordova build android --prod --release"`
2) `"signUnsignedApk": "jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore /Users/ericdixon/Documents/projects/keystore/speak-write-release-key.keystore platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk speak-write-alias"`
3) `"zipAlignApk": "~/Library/Android/sdk/build-tools/28.0.3/zipalign -v 4 platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk SpeakWrite_v0.0.5.apk"` (Update the version of the apk)

The `.apk` should be ready to deploy to the developer console.