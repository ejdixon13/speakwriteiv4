export const LOCALE: string = 'en-GB';
export const DEFAULT_SPEECH_RATE: number = 10;
export const DEFAULT_SPEECH_PITCH: number = .9;
export const IS_FULLY_SUPPORTED_ANDROID_VERSION = (version: string) => parseInt(version, 10) >= 8;
