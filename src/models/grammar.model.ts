import { RangeStatic } from 'quill';


export class WordPunct {
  constructor(
    public value: string,
    public position: RangeStatic
  ) { }

  get endIndex() {
    return this.position.index + this.position.length;
  }
}

export class Sentence {
  constructor(
    public value: string,
    public range: RangeStatic
  ) {}
}

export class Paragraph {

  constructor(public sentences: Sentence[], public range: RangeStatic) {}
  get value() {
    return this.sentences
      .map(s => s.value)
      .reduce((result: string, s: string) => result += s);
  }
}

export const PUNCTUATION = [
  ',', '-', ';', ':', '\'', '"'

];

export const VALID_SENTENCE_END = [
  '!', '!!', '!!!', '?', '?!', '??', '???', '.', '...', ';', '…'
];
export const VALID_SENTENCE_END_REG_EX = /[.!?;]+/g;

export const VALID_WORD = /^[a-z']+$/i;

export const IGNORE_PUNCTUATION_REGEX = /[^,;:" ]+/g;
export const IGNORE_ALL_PUNCTUATION_REGEX = /[^,.!?";)( ]+/g;
export const MATCH_ALL_PUNCTUATION_REGEX = /[,.!?";)(]+/g;
