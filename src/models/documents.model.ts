

import {Delta} from 'quill';

export class DocumentModel {
  constructor(
    public id: number,
    public folderId: number | null,
    public title: string,
    public doc: Delta,
    public previewText: string
  ) {}
}


export class DocumentDBModel {
  constructor(
    public id: number,
    public folderId: number | null,
    public title: string,
    public DocDBData: DocDBData
  ) {}
}

export interface DocDBData {
  value: string;
  marks: Array<any>;
  history: Array<any>;
}
