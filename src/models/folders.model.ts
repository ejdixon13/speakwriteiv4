
export class FolderModel {
  constructor(
    public id: number,
    public title: string,
    public folders: Array<FolderModel>
  ) {}
}

/***
 *
 * home_folder
 *    file_1
 *    file_2
 *    folder_1
 *      file_3
 *      file_4
 *
 *
 *
 *
 *
 */
