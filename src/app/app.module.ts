import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutPage } from './about/about';
import { CommandListPage } from './commandList/command-list';
import { QuillEditorModule } from './components/quill-editor/quillEditor.module';
import { EditorPage } from './editor/editor';
import { HomePage } from './home/home';
import { SettingsPage } from './settings/settings';
import { TutorialPage } from './tutorial/tutorial';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { IonicStorageModule } from '@ionic/storage';
import { TextToSpeech } from '@ionic-native/text-to-speech/ngx';
import { RecordingService } from './services/recording.service';
import { CommandService } from './services/command.service';
import { JarvisService } from './services/jarvis.service';
import { WordsService } from './services/words.service';
import { DocumentService } from './services/document.service';
import { FolderService } from './services/folder.service';
import { Insomnia } from '@ionic-native/insomnia/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { UserService } from './services/user.service';
import { QuillEditorService } from './components/quill-editor/quill-editor.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { SettingsService } from './services/settings.service';
import { PaidVsFreeService } from './services/paidVsFree.service';
import { AdService } from './services/ad.service';
import { AdMobPro } from '@ionic-native/admob-pro/ngx';
import { Device } from '@ionic-native/device/ngx';
import { CardPopover } from './popovers/card-popover';
import { SynonymPopoverComponent } from './popovers/synonym-popover';
import { UpgradePage } from './upgrade/upgrade';
import { ThesaurusService } from './services/thesaurus.service';
import { HTTP } from '@ionic-native/http/ngx';
import { DefinitionPopoverComponent } from './popovers/definition-popover';
import { TutorialAnimatedSlideComponent } from './tutorial/tutorial-animated-slide';

@NgModule({
  declarations: [
    AppComponent,
    AboutPage,
    CommandListPage,
    EditorPage,
    HomePage,
    SettingsPage,
    UpgradePage,
    TutorialPage,
    TutorialAnimatedSlideComponent,
    CommandListPage,
    CardPopover,
    SynonymPopoverComponent,
    DefinitionPopoverComponent
  ],
  entryComponents: [
    AboutPage,
    EditorPage,
    HomePage,
    TutorialPage,
    TutorialAnimatedSlideComponent,
    SettingsPage,
    UpgradePage,
    CommandListPage,
    CardPopover,
    SynonymPopoverComponent,
    DefinitionPopoverComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    QuillEditorModule
  ],
  providers: [
    AdMobPro,
    AdService,
    AndroidPermissions,
    Clipboard,
    CommandService,
    Device,
    DocumentService,
    FolderService,
    Insomnia,
    JarvisService,
    Keyboard,
    PaidVsFreeService,
    QuillEditorService,
    RecordingService,
    SettingsService,
    SplashScreen,
    SpeechRecognition,
    StatusBar,
    TextToSpeech,
    UserService,
    WordsService,
    InAppPurchase2,
    HTTP,
    ThesaurusService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
