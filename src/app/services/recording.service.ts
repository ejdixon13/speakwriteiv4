import { ApplicationRef, Injectable } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';


@Injectable({
  providedIn: 'root',
})
export class RecordingService {

  TAG: string = 'JARVIS APP';


  constructor(private ref: ApplicationRef, private androidPermissions: AndroidPermissions) {}


  startListening(callback): void {
    (window as any).ContinuousVoiceRecognizer.startRecognize((matches) => {
        this.ref.tick();
        callback(matches);
        console.log('Voice Recognition Success:' + matches);
      }, (err) => {
        console.log('Voice Recognition Error: ' + err, 'background: #222; color: #bada55');
      }, 1000);
  }

  private checkBluetoothPermissions() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.BLUETOOTH).then(
      result => {
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.BLUETOOTH])
          .then(value => {
            console.log(value);
          });
        console.log('Has permission?', result.hasPermission);
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.BLUETOOTH)
    );

    // TODO: move to shared method
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS).then(
      result => {
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS])
          .then(value => {
            console.log(value);
          });
        console.log('Has permission?', result.hasPermission);
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS)
    );

  }

  // startListening() {
  //   (<any>window).ContinuousVoiceRecognizer.startRecognize((matches) => {
  //     console.log(this.TAG + matches);
  //     this.parseCommand(matches);
  //
  //     this.allMatches = matches;
  //     this.ref.tick();
  //   }, function(err) {
  //     console.log('Error: ' + err);
  //   }, 1000);
  // }

  stopListening(callback): void {
      (window as any).ContinuousVoiceRecognizer.stopRecognize(() => {
        this.ref.tick();
        callback(null);
      }, (err) => {
        console.log('Error: ' + err);
      }, 1000);
  }

  setPlaybackVolume(volume, callback): void {
    (window as any).ContinuousVoiceRecognizer.setPlaybackVolume(() => {
      console.log('after playback volume set');
      this.ref.tick();
      callback(null);
    }, (err) => {
      console.log('Error: ' + err);
    }, volume);
  }



}
