import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { StorageItems } from './storage-items.enum';
import { AdService } from './ad.service';
import { Subject, BehaviorSubject } from 'rxjs';
export enum REWARD {
    WORD_LIMIT_INCREASE
}

const DEFAULT_WORD_LIMIT = 200;
const DEFAULT_WORD_INCREASE_VALUE = 225;

@Injectable({
    providedIn: 'root',
  })
export class PaidVsFreeService {

    // Observable string sources
    private _isPaid = new BehaviorSubject<boolean>(false);

    // Observable string streams
    isPaid$ = this._isPaid.asObservable();
    constructor(private storage: Storage, private adService: AdService) {
        this.setIsPaid();
     }

    setIsPaid(isPaid?: boolean) {
        this._isPaid.next(isPaid);
    }

    giveReward(reward: REWARD) {
        if (reward === REWARD.WORD_LIMIT_INCREASE) {
            this.increaseWordLimit();
        }
    }

    setDefaultWordLimit() {
        this.storage.get(StorageItems.WORD_LIMIT.toString())
            .then(value => {
                if (!value) {
                    this.setWordLimit(DEFAULT_WORD_LIMIT);
                }
            });
    }
    setWordLimit(limit: number = DEFAULT_WORD_LIMIT) {
        this.storage.set(StorageItems.WORD_LIMIT.toString(), limit);
    }

    async addToWordsUsed(numWordsToAdd: number) {
        const numWordsUsed = await this.storage.get(StorageItems.WORDS_USED.toString());
        const totalWordsUsed = numWordsUsed ? numWordsUsed + numWordsToAdd : numWordsToAdd;
        await this.storage.set(StorageItems.WORDS_USED.toString(), totalWordsUsed);
        const wordLimit = await this.storage.get(StorageItems.WORD_LIMIT.toString());
        console.log('PaidVsFreeService: word limit: ' + wordLimit);
        console.log('PaidVsFreeService: words used: ' + totalWordsUsed);
        const overWordIncreaseValue = wordLimit ? (totalWordsUsed - wordLimit) > DEFAULT_WORD_INCREASE_VALUE : true;
        if (totalWordsUsed > wordLimit) {
            console.log('PaidVsFreeService: prepare and show interstitial');
            await this.adService.prepareInterstitialAd();
            this.increaseWordLimit(overWordIncreaseValue ? totalWordsUsed : null);
        }
    }

    private async increaseWordLimit(oldLimit?: number) {
        if (oldLimit) {
            this.setWordLimit(oldLimit += DEFAULT_WORD_INCREASE_VALUE);
        } else {
            this.storage.get(StorageItems.WORD_LIMIT.toString())
                .then(value => {
                    if (!value) {
                        this.setDefaultWordLimit();
                    } else if (!isNaN(value)) {
                        this.setWordLimit(value += DEFAULT_WORD_INCREASE_VALUE);
                    }
                });
        }
    }

}
