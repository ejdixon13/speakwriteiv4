import {Injectable, ApplicationRef} from '@angular/core';
import {Storage} from '@ionic/storage';
import { StorageItems } from './storage-items.enum';


@Injectable({
  providedIn: 'root',
})
export class UserService {

  constructor(private storage: Storage) {}

  hasAppBeenOpenedPreviously(): Promise<boolean> {
    return this.storage.get(StorageItems.APP_PREVIOUSLY_OPENED.toString()).then((value: any) => {
      return !!value;
    });
  }

  setAppBeenOpenedPreviously(hasAppBeenOpenedPreviously: boolean) {
    return this.storage.set(StorageItems.APP_PREVIOUSLY_OPENED.toString(), hasAppBeenOpenedPreviously)
      .then((value: any) => value);
  }

}
