import { Injectable, Inject } from '@angular/core';
import { DEFAULT_SPEECH_RATE, DEFAULT_SPEECH_PITCH } from '../../models/app.const';
import { Observable, BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';
import { StorageItems } from './storage-items.enum';

export class Settings {
    playbackSpeed: number;
    playbackPitch: number;
    playbackImmediately: boolean;
    fontSizeWhileRecording: number;

    constructor(settings: Partial<Settings> = new Settings({})) {
        this.playbackSpeed = settings.playbackSpeed || DEFAULT_SPEECH_RATE;
        this.playbackPitch = settings.playbackPitch || DEFAULT_SPEECH_PITCH;
        this.playbackImmediately = settings.playbackImmediately || false;
        this.fontSizeWhileRecording = settings.fontSizeWhileRecording || 3;
    }

    get TTSPlaybackSpeed() {
        const speed = this.playbackSpeed * .1;
        return Math.round(speed * 10) / 10; // to get single decimal place
    }

    get TTSPlaybackPitch() {
        const pitch = this.playbackPitch * .1;
        return Math.round(pitch * 10) / 10;
    }
}

@Injectable({
    providedIn: 'root',
  })
export class SettingsService {

    private _state$: BehaviorSubject<Settings>;
    state$: Observable<Settings>;

    constructor(@Inject(Storage) private storage: Storage) {
        this._state$ = new BehaviorSubject(new Settings());
        this.state$ = this._state$.asObservable();
        this.storage.get(StorageItems.SETTINGS.toString())
            .then(settings => {
                if (!settings) {
                    this._state$.next(new Settings());
                } else {
                    this._state$.next(new Settings(settings));
                }
            });

    }

    saveSettings(settings: Settings) {
        this.storage.set(StorageItems.SETTINGS.toString(), settings)
            .then(() => {
                this._state$.next(new Settings(settings));
            });
    }
}
