import {Injectable, ApplicationRef} from '@angular/core';
import {Subscription} from 'rxjs';
import {WordsService} from '../services/words.service';

@Injectable({
  providedIn: 'root',
})
export class JarvisService {

  TAG: string = 'JARVIS APP';

  allMatches: Array<string>;

  currentSentence: string;

  recordingSubscription: Subscription;

  constructor(private ref: ApplicationRef,  private wordsService: WordsService) {}

  startListening() {
    // this.recordingSubscription = this.recordingService
    //   .startListening()
    //   .subscribe((matches)=>{
    //     this.ref.tick();
    //     this.commandService.parseCommand(matches);
    //   });
  }

  stopListening() {
    // this.recordingSubscription.unsubscribe();
    // this.recordingService
    //   .stopListening()
    //   .subscribe();
  }



}
