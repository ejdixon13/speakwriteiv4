import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import * as _ from 'lodash';
import {each, find} from 'lodash';
import {FolderModel} from '../../models/folders.model';
import { StorageItems } from './storage-items.enum';

@Injectable({
  providedIn: 'root',
})
export class FolderService {
// Observable string sources
//   private docsRefreshedSource = new Subject<Array<DocumentModel>>();
//
//   // Observable string streams
//   docsRefreshed$ = this.docsRefreshedSource.asObservable();



  constructor(private storage: Storage) {}

  createNewFolder(folderTitle: string, parentFolderId: number = StorageItems.ROOT_FOLDER_ID) {
    const rootFolderPromise = this.getRootFolder();
    const currentFolderIdSequencePromise = this.getFolderIdSequence();

    return Promise.all([rootFolderPromise, currentFolderIdSequencePromise]).then((values) => {
      const rootFolder = values[0];
      const folderId = values[1];
      folderTitle = folderTitle ? folderTitle : 'Folder-' + folderId;

      const newFolder: FolderModel = new FolderModel(folderId, folderTitle, []);

      const parentFolder: FolderModel = this.getFolder(rootFolder, parentFolderId);
      parentFolder.folders.push(newFolder);

      return this.setRootFolder(rootFolder).then(() => {
        console.log('New Folder \'' + folderTitle + '\' Saved!');
        return newFolder;
      });
    });
  }

  getFolder(containingFolder: FolderModel, folderId: number) {
    let folderToReturn = null;
    if (folderId === containingFolder.id) {
      return containingFolder;
    }
    function recurseFolder(containingFolder) {
      if (folderId === containingFolder.id) {
        folderToReturn = containingFolder;
        return;
      }
      each(containingFolder.folders,  recurseFolder);
    }
    recurseFolder(containingFolder);
    return folderToReturn;
  }

  updateFolderTitle(newFolderTitle: string) {
    // return this.getAllDocuments().then((documents: Array<DocumentModel>) => {
    //   let documentModel: DocumentModel = _.find(documents, {id: id});
    //   documentModel.doc = doc;
    //   return this.setAllDocuments(documents).then(()=>{
    //     return documentModel;
    //   });
    // })
  }

  deleteFolder(id: any, parentFolderId) {
    if (id === StorageItems.ROOT_FOLDER_ID) {
      console.error('Attempting to delete Root Folder!');
      return;
    }

    return this.getRootFolder().then((rootFolder) => {

      const parentFolder: FolderModel = this.getFolder(rootFolder, parentFolderId);
      parentFolder.folders = parentFolder.folders
        .filter((folder) => folder.id !== id);

      return this.setRootFolder(rootFolder).then(() => {
        console.log('Folder \'' + id + '\' Deleted!');
        return id;
      });
    });
  }

  getFolderIdSequence() {
    return this.storage.get(StorageItems.FOLDER_ID_SEQUENCE.toString())
      .then((folderIdSequence: number | null) => {
          if (!folderIdSequence) {
            folderIdSequence = StorageItems.ROOT_FOLDER_ID;
          }
          return this.storage.set(StorageItems.FOLDER_ID_SEQUENCE.toString(), folderIdSequence + 1)
          .then((savedFolderIdSequence) => savedFolderIdSequence);
      });
  }

  getRootFolder() {
    return this.storage.get(StorageItems.ROOT_FOLDER.toString())
      .then((rootFolder: FolderModel | null) => {
        if (!rootFolder) {
          return this.storage.set(StorageItems.ROOT_FOLDER.toString(), new FolderModel(StorageItems.ROOT_FOLDER_ID, 'root', []))
            .then((savedRootFolder: FolderModel) => savedRootFolder);
        }
        return rootFolder;
      });
  }

  setRootFolder(rootFolder: FolderModel) {
    if (rootFolder) {
      return this.storage.set(StorageItems.ROOT_FOLDER.toString(), rootFolder);
    }
  }

}
