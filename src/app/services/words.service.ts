import {Injectable, ApplicationRef} from '@angular/core';
import {FormControl} from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class WordsService {

  TAG: string = 'JARVIS APP';

  words: FormControl = new FormControl('Sample Sentence');

  position: number = 0;

  constructor(private ref: ApplicationRef) {}

  addWords(words: string) {
    const formattedWords = this.isValidSetenceEnd(words[0]) ? words : ' ' + words;
    this.addWordsAtCursorPosition(formattedWords);
  }

  addWordsAtCursorPosition(words: string) {
    // let firstHalf = this.words.value.substr(0, this.richTextElementService.cursorPosition);
    // let secondHalf = this.words.value.substr(this.richTextElementService.cursorPosition);
    //
    // this.words.setValue(firstHalf + words + secondHalf);
    // this.updateRichTextElement();
  }

  getWords(): FormControl {
    return this.words;
  }

  setWords(words: string) {
    this.words.setValue(words);
    this.updateRichTextElement();
  }

  updateRichTextElement() {
    // this.richTextElementService.updateElement();
    // this.ref.tick();
  }

  getLastSentence() {
    const endPtr = this.words.value.length - 2; // accounts for period
    let beginningPtr = endPtr;
    let sentenceBeginning = false;
    while (!sentenceBeginning) {
      beginningPtr--;
      if ( (endPtr < 0) || (beginningPtr < 0) ) { return; }

      if (this.isValidSetenceEnd(this.words.value[beginningPtr])) {
        sentenceBeginning = true;
      }

    }

    return this.words.value.substr(beginningPtr, endPtr);
  }

  getLastParagraph() {

  }

  getContextOfWord(word) {

  }

  private isValidSetenceEnd(char) {
    return char === '.' || char === '!' || char === '?';
  }

  deleteLastSentence() {
    this.words.setValue(this.words.value.substr(0, this.words.value.indexOf(this.getLastSentence()) + 1));
    this.updateRichTextElement();
  }

  getPassiveVoice(script) {
    /**
      var fs = this;
      if (script.text.length > maxScanLen) {
        return[];
      }
      var contextElem = script.text.match(/\s(is|are|was|were|be|been|being)\s([a-z]{2,30})\b(\sby\b)?/gi);
      return contextElem ? contextElem.filter(function(segment) {
        var parts = segment.match(/([a-z]+)\b(\sby\b)?$/i);
        if (!parts) {
          return false;
        }
        var part = parts[1];
        var getPassiveVoices = part.match(/ed$/) || void 0 !== out.default[part.toLowerCase()];
        return getPassiveVoices;
      }).map(function(requestUrl) {
        return requestUrl.replace(/^\s/, "");
      }).map(function(lines, callback, details) {
        return fs.buildTreeFragment(self.default.tokens.passiveVoice, lines, fs.getPassiveIndex(script, lines, callback, details));
      }).map(function(opt) {
        return fs.addTrailingSpaceSetting(opt, script);
      }) : [];
     **/

  }

  getAdverbs() {
    /**
      var dojo = this;
      return s.items.filter(function(script) {
        return script.text.match(/ly$/);
      }).filter(function(target) {
        return void 0 === obj.default[target.text.toLowerCase()];
      }).map(function(node) {
        return dojo.buildTreeFragment(self.default.tokens.adverb, node.text, node.startIndex);
      }).map(function(tag) {
        return dojo.addTrailingSpaceSetting(tag, s);
      });
     **/
  }

  getItemDelimiter(dataAndEvents) {
    switch (dataAndEvents) {
      case 'root':
        return '[\n]+';
      case 'paragraph':
        return '[.?!]{1,2}["\u201d\'\\)]?(?:\\s|$)';
      case 'sentence':
        return '[^\\w\'-]';
      default:
        return '';
    }
  }

  getSubType(deepDataAndEvents) {
    switch (deepDataAndEvents) {
      case 'root':
        return 'paragraph';
      case 'paragraph':
        return 'sentence';
      case 'sentence':
        return 'word';
      default:
        return '';
    }
  }


}
