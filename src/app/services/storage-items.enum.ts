export enum StorageItems {
    SETTINGS = 'settings',
    DOCUMENTS = 'documents',
    FOLDER_ID_SEQUENCE = 'folderIdSequence',
    ROOT_FOLDER = 'rootFolder',
    ROOT_FOLDER_ID = 1,
    APP_PREVIOUSLY_OPENED = 'appBeenOpenedPreviously',
    IS_PAID = 'isPaid',
    WORD_LIMIT = 'wordLimit',
    WORDS_USED = 'wordsUsed'
}
