import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import * as _ from 'lodash';
import { maxBy } from 'lodash';
import * as Delta from 'quill-delta';
import { Observable, from, of, Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { DocumentModel } from '../../models/documents.model';
import { StorageItems } from './storage-items.enum';



const DOC_LIMIT = 300;

@Injectable({
  providedIn: 'root',
})
export class DocumentService {
// Observable string sources
  private docsRefreshedSource = new Subject<Array<DocumentModel>>();

  // Observable string streams
  docsRefreshed$ = this.docsRefreshedSource.asObservable();



  constructor(private storage: Storage) {}

  // Service message commands
  refreshDocs() {
    this.getAllDocuments().then((documents) => {
      this.docsRefreshedSource.next(documents);
    });
  }

  createNewDocument(documentTitle: string, folderId: number = 1) {
    return this.getAllDocuments().then((documents: Array<DocumentModel>) => {
      const documentId = (documents && documents.length > 0) ? maxBy(documents, 'id').id + 1 : 1;
      documentTitle = documentTitle ? documentTitle : 'Untitled-' + documentId;
      if (!documents) {
        documents = [];
      }
      const newDocument: DocumentModel = new DocumentModel(documentId, folderId, documentTitle, new Delta({ ops: [{ insert: '' }] }), '');
      documents.push(newDocument);

      return this.setAllDocuments(documents).then(() => {
        console.log('New Document \'' + documentTitle + '\' Saved!');
        return newDocument;
      });
    });

  }

  updateDocument(id: number, doc: Delta, previewText: string): Observable<DocumentModel | any[]> {
    return from(this.getAllDocuments())
      .pipe(
        switchMap((documents: Array<DocumentModel>) => {
          const documentModel: DocumentModel = _.find(documents, { id: id });
          documentModel.doc = doc;
          documentModel.previewText = previewText;
          return from(this.setAllDocuments(documents))
            .pipe(
              switchMap(() => of(documentModel))
            );
        })
      );
  }

  deleteDocument(id: any) {
    return this.getAllDocuments().then((documents: Array<DocumentModel>) => {
      return this.setAllDocuments(documents.filter(doc => doc.id !== id)).then(() => {
        return document;
      });
    });
  }

  getAllDocuments() {
    return this.storage.get(StorageItems.DOCUMENTS.toString()).then((documents: Array<DocumentModel>) => {
      return documents;
    });
  }

  setAllDocuments(documents: DocumentModel[]) {
    return this.storage.set(StorageItems.DOCUMENTS.toString(), documents);
  }

  getDocument(id: any) {
    return this.getAllDocuments()
      .then((documents: Array<DocumentModel>) => _(documents).find({id: +id}));
  }

}
