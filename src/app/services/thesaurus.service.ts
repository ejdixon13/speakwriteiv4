import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { Subject, from, Observable, BehaviorSubject, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { filter, map, forEach } from 'lodash';

// const API_KEY = '30aa473540msh2ce92b383a03677p11d06fjsn95ed7f46bcaf';
@Injectable({
    providedIn: 'root',
})
export class ThesaurusService {

    // Observable string sources
    private synonymsRefreshedSource = new BehaviorSubject<string[]>([]);
    private definitionsRefreshedSource = new BehaviorSubject<string[]>([]);

    // Observable string streams
    synonymsRefreshed$ = this.synonymsRefreshedSource.asObservable();
    definitionsRefreshed$ = this.definitionsRefreshedSource.asObservable();
    constructor(private http: HTTP) {
    }

    refreshSynonymList(word: string): Observable<any> {
        // const synArray = ['word', 'sheep', 'cow', 'foolish',
        //     'dull', 'stupid', 'cool', 'creative', 'tree', 'special', 'spoon', 'slight',
        //     'dim-witted', 'feebleminded', 'moronic', 'sheepish', 'silly', 'serious',
        //     'goat', 'chicken', 'nother word', 'longer word', 'longest word possible',
        //     'silly', 'serious',
        //     'goat', 'chicken', 'nother word', 'longer word', 'longest word possible'
        //     , 'silly', 'serious',
        //     'goat', 'chicken', 'nother word', 'longer word', 'longest word possible'];
        // this.synonymsRefreshedSource.next(synArray);
        // return of(['word', 'sheep', 'cow', 'goat', 'chicken', 'nother word', 'longer word', 'longest word possible']);
        // const url = 'https://wordsapiv1.p.mashape.com/words/' + word + '/synonyms';
        // 'X-Mashape-Key': API_KEY
        // data.definitionData.definitions[0].synonyms
        return this.getPageData(word)
            .pipe(
                tap(value => this.parseSynonyms(value))
            );
                // WORDS API
                // tap(value => this.synonymsRefreshedSource.next(JSON.parse(value.data).synonyms))
    }

    private getPageData(word: string): Observable<any> {
        const url = 'https://tuna.thesaurus.com/pageData/' + word;
        return from(this.http.get(url, {}, {}));
    }

    refreshDefinitionsList(word: string): Observable<any> {
        // const synArray = ['word', 'sheep', 'cow', 'foolish',
        //     'dull', 'stupid', 'cool', 'creative', 'tree', 'special', 'spoon', 'slight',
        //     'dim-witted', 'feebleminded', 'moronic', 'sheepish', 'silly', 'serious',
        //     'goat', 'chicken', 'nother word', 'longer word', 'longest word possible',
        //     'silly', 'serious',
        //     'goat', 'chicken', 'nother word', 'longer word', 'longest word possible'
        //     , 'silly', 'serious',
        //     'goat', 'chicken', 'nother word', 'longer word', 'longest word possible'];
        // this.synonymsRefreshedSource.next(synArray);
        // return of(['word', 'sheep', 'cow', 'goat', 'chicken', 'nother word', 'longer word', 'longest word possible']);
        // const url = 'https://wordsapiv1.p.mashape.com/words/' + word + '/synonyms';
        // 'X-Mashape-Key': API_KEY
        // data.definitionData.definitions[0].synonyms
        return this.getPageData(word)
            .pipe(
                tap(value => this.parseDefinitions(value))
            );
                // WORDS API
                // tap(value => this.synonymsRefreshedSource.next(JSON.parse(value.data).synonyms))
    }

    private parseSynonyms(value: any) {
        let synonymArray = [];
        const rawDefinitions = JSON.parse(value.data).data.definitionData.definitions;
        if (rawDefinitions.length > 0) {
            forEach(rawDefinitions, rawDefinition => {
                synonymArray = synonymArray.concat(filter(rawDefinition.synonyms, syn => +syn.similarity === 100));
            });
            forEach(rawDefinitions, rawDefinition => {
                synonymArray = synonymArray.concat(filter(rawDefinition.synonyms, syn => +syn.similarity < 100));
            });
            this.synonymsRefreshedSource.next(map(synonymArray, match => match.term));
        }
        // const rawSynonyms = JSON.parse(value.data).data.definitionData.definitions[0].synonyms;
        // const closestMatches = filter(rawSynonyms, syn => +syn.similarity >= 50);
        // const synonymArray = map(closestMatches, match => match.term);
        // this.synonymsRefreshedSource.next(synonymArray);
    }

    private parseDefinitions(value: any) {
        const rawDefinitions = JSON.parse(value.data).data.definitionData.definitions;
        if (rawDefinitions.length > 0) {
            this.synonymsRefreshedSource.next(
                map(rawDefinitions, definitionObj => '(' + definitionObj.pos + ') ' + definitionObj.definition)
            );
        }
    }
}
