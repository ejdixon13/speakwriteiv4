import { Injectable } from '@angular/core';
import { AdMobPro } from '@ionic-native/admob-pro/ngx';

const IS_TEST_MODE = false;

const ADMOBID = {
    banner: 'ca-app-pub-3817455050750068/9696087192', // 'ca-app-pub-3940256099942544/6300978111',
    reward: 'ca-app-pub-3817455050750068/8387997427', // 'ca-app-pub-3940256099942544/5224354917',
    interstitial: 'ca-app-pub-3817455050750068/8391414544'
};
  
const BANNER_AD_POSITION = 8; // BOTTOM
@Injectable({
    providedIn: 'root',
  })
export class AdService {

    constructor(private admob: AdMobPro) { }

    prepareBannerAd() {
        this.admob.createBanner({
            adId: ADMOBID.banner,
            isTesting: IS_TEST_MODE,
            autoShow: true,
            position: BANNER_AD_POSITION
        });
    }
    
    showBannerAd() {
        this.admob.showBanner(BANNER_AD_POSITION);
    }
    hideBannerAd() {
        this.admob.hideBanner();
    }
    
    prepareVideoAd() {
        return this.admob.prepareRewardVideoAd({
            adId: ADMOBID.reward,
            isTesting: IS_TEST_MODE,
            autoShow: false
          });
    }
    showVideoAd() {
        if (AdMobPro) {
            this.admob.showRewardVideoAd();
        }
    }

    prepareInterstitialAd() {
        return this.admob.prepareInterstitial({
            adId: ADMOBID.interstitial,
            isTesting: IS_TEST_MODE,
            autoShow: true
        });
    }
    
    // Dont really need this since we auto show
    showInterstitialAd() {
        if (AdMobPro) {
            this.admob.showInterstitial();
        }
    }

}
