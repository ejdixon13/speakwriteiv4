import { Component, Inject } from '@angular/core';
import {
  PLAYBACK,
  PLAY_LAST_SENTENCE,
  PLAY_LAST_PARAGRAPH,
  PLAY_LAST_WORD,
  PLAY_ALL,
  SCRATCH_THAT,
  DELETE_LAST_SENTENCE,
  ERASE_LAST_SENTENCE,
  DELETE_LAST_WORD,
  ERASE_LAST_WORD,
  NAVIGATE_ONE_WORD_BACK,
  NAVIGATE_ONE_WORD_FORWARD,
  GO_TO_END,
  GO_TO_THE_END,
  OPEN_QUOTE, CLOSE_QUOTE,
  SEMICOLON, COLON,
  CAPITALIZE,
  UNCAPITALIZE, GO_TO_X, NAVIGATE_ONE_SENTENCE_BACK,
  NAVIGATE_ONE_SENTENCE_BACK_2, NAVIGATE_ONE_SENTENCE_BACK_3, NAVIGATE_ONE_SENTENCE_FORWARD, DEFINE, DEFINITION,
  SYNONYM,
  SYNONYMS
} from '../services/command.service';
import { Device } from '@ionic-native/device/ngx';
import { IS_FULLY_SUPPORTED_ANDROID_VERSION } from '../../models/app.const';

const COMMAND_LIST = [
  {
    category: 'Playback',
    color: 'blue',
    commands: [
      {
        name: PLAYBACK,
        description: 'Reads back sentence before the cursor or that the cursor intersects',
        alternatePhrases: [PLAY_LAST_SENTENCE]
      },
      {
        name: PLAY_LAST_PARAGRAPH,
        description: 'Reads back paragraph before the cursor or that the cursor intersects',
        alternatePhrases: []
      },
      {
        name: PLAY_LAST_WORD,
        description: 'Reads back last word',
        alternatePhrases: []
      },
      {
        name: PLAY_ALL,
        description: 'Reads back all text in document',
        alternatePhrases: []
      }
    ]
  },
  {
    category: 'Edit',
    color: 'orange',
    commands: [
      {
        name: SCRATCH_THAT,
        description: 'Removes last captured phrase',
        alternatePhrases: []
      },
      {
        name: DELETE_LAST_SENTENCE,
        description: 'Deletes last sentence before the cursor or that the cursor intersects',
        alternatePhrases: [ERASE_LAST_SENTENCE]
      },
      {
        name: DELETE_LAST_WORD,
        description: 'Deletes highlighted word',
        alternatePhrases: [ERASE_LAST_WORD]
      },
      {
        name: OPEN_QUOTE,
        description: 'Inserts \'"\' with no space after',
        alternatePhrases: []
      },
      {
        name: CLOSE_QUOTE,
        description: 'Inserts \'"\' with no space before or after',
        alternatePhrases: []
      },
      {
        name: CAPITALIZE,
        description: 'Capitalizes first letter of current word',
        alternatePhrases: []
      },
      {
        name: UNCAPITALIZE,
        description: 'Uncapitalizes first letter of current word',
        alternatePhrases: []
      }
    ]
  },
  {
    category: 'Navigation',
    color: 'green',
    commands: [
      {
        name: GO_TO_X + ' (word)',
        description: 'Navigates to specified word on screen',
        alternatePhrases: []
      },
      {
        name: GO_TO_END,
        description: 'Navigates to the end of the document',
        alternatePhrases: [GO_TO_THE_END]
      },
      {
        name: NAVIGATE_ONE_WORD_BACK,
        description: 'Navigates one word backward',
        alternatePhrases: []
      },
      {
        name: NAVIGATE_ONE_SENTENCE_BACK,
        description: 'Navigates one sentence backward',
        alternatePhrases: [NAVIGATE_ONE_SENTENCE_BACK_2, NAVIGATE_ONE_SENTENCE_BACK_3]
      },
      {
        name: 'BACK X WORDS/SENTENCES (X IS 2, 3, OR 4)',
        description: 'Navigates x words/sentences backward',
        alternatePhrases: []
      },
      {
        name: NAVIGATE_ONE_WORD_FORWARD,
        description: 'Navigates one word forward',
        alternatePhrases: []
      },
      {
        name: NAVIGATE_ONE_SENTENCE_FORWARD,
        description: 'Navigates one sentence forward',
        alternatePhrases: []
      },
      {
        name: 'NEXT X WORDS/SENTENCES (X IS 2, 3, OR 4)',
        description: 'Navigates x words/sentences forward',
        alternatePhrases: []
      }
      
    ]
  },
  {
    category: 'Word Utilities',
    color: 'tertiary',
    commands: [
      {
        name: DEFINE,
        description: 'Shows definitions for currently highlighted word',
        alternatePhrases: [DEFINITION]
      },
      {
        name: SYNONYM,
        description: 'Shows synonyms for currently highlighted word',
        alternatePhrases: [SYNONYMS]
      },
    ]
  }
];

@Component({
  selector: 'page-command-list',
  templateUrl: 'command-list.html',
  styleUrls: ['./command-list.scss']
})
export class CommandListPage {

  commandList = COMMAND_LIST;

  constructor(private device: Device) {}


  get isFullySupportedAndroidVersion() {
    return IS_FULLY_SUPPORTED_ANDROID_VERSION(this.device.version);
  }

  get filteredCommands() {
    return COMMAND_LIST.filter(command => {
      return IS_FULLY_SUPPORTED_ANDROID_VERSION(this.device.version) || (command.category !== 'Navigation');
    });
  }
}
