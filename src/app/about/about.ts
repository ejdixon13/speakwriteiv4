import { Component } from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
  styleUrls: ['./about.scss']
})
export class AboutPage {
  searchTerm: FormControl = new FormControl();

}
