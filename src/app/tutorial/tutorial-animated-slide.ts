import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { timer } from 'rxjs';
import { tap, delay } from 'rxjs/operators';

const EDITOR_PART_1 = 'Once upon a time<span class="pink-background">,</span>';
const EDITOR_PART_1_NO_CURSOR = 'Once upon a time, ';
const EDITOR_PART_2 = ' there was a <span class="pink-background">prince</span>';
const EDITOR_PART_2_NO_CURSOR = 'Once upon a time, there was a prince';
const EDITOR_PART_3 = ' there was a <span class="pink-background">princess</span>';
const EDITOR_PART_3_NO_CURSOR = 'Once upon a time, there was a princess';
const EDITOR_PART_4 = ' who lived on the moon<span class="pink-background">.</span>';
const EDITOR_PART_5 = 'Once upon a time, there was a <span class="pink-background">princess</span> who lived on the moon.';
const EDITOR_PART_6 = 'Once upon a time, there was a killer <span class="pink-background">robot</span> who lived on the moon.';
const EDITOR_PART_7 = 'Once upon a time, there was a killer <span class="pink-background">robot</span> who lived on the moon.';
const EDITOR_PART_8 = 'Once upon a time, there was a killer <span class="pink-background">cyborg</span> who lived on the moon.';
const EDITOR_PART_9 = 'Once upon a time, there was a killer cyborg who lived on the moon<span class="pink-background">.</span>';

const SPOKEN_WORDS_PART_1 = 'Once upon a time comma';
const SPOKEN_WORDS_PART_2 = 'There was a prince';
const SPOKEN_WORDS_PART_3 = 'Scratch that';
const SPOKEN_WORDS_PART_4 = 'There was a princess';
const SPOKEN_WORDS_PART_5 = 'Who lived on the moon period';
const SPOKEN_WORDS_PART_6 = 'Go to princess';
const SPOKEN_WORDS_PART_7 = 'Replace with killer robot';
const SPOKEN_WORDS_PART_8 = 'Synonym';
const SPOKEN_WORDS_PART_9 = 'Replace with cyborg';
const SPOKEN_WORDS_PART_10 = 'Go to end';
const SPOKEN_WORDS_PART_11 = 'Read back';
@Component({
    selector: 'tutorial-animated-slide',
    templateUrl: 'tutorial-animated-slide.html',
    styleUrls: [
        './tutorial.scss',
        '../popovers/synonym-popover.scss'
    ]
})

export class TutorialAnimatedSlideComponent implements OnInit {
    nonAnimatedEditorContent: string;
    animatedEditorContent: SafeHtml;
    showAnimatedEditorText: boolean = false;
    showAnimatedSpokenWords: boolean = false;
    spokenWords: string = '';
    showMockSynonymPopover: boolean = false;
    showReadBack: boolean = false;
    constructor(
        private sanitizer: DomSanitizer) { }

    
    ngOnInit() {
        this.startSpokenWordsPlayscript();
        this.startEditorPlayscript();
    }

    private startSpokenWordsPlayscript() {
        timer(1000)
        .pipe(
            tap(() => this.updateSpokenWordsContent(SPOKEN_WORDS_PART_1)),
            delay(4000),
            tap(() => this.updateSpokenWordsContent(SPOKEN_WORDS_PART_2)),
            delay(4000),
            tap(() => this.updateSpokenWordsContent(SPOKEN_WORDS_PART_3)),
            delay(4000),
            tap(() => this.updateSpokenWordsContent(SPOKEN_WORDS_PART_4)),
            delay(4000),
            tap(() => this.updateSpokenWordsContent(SPOKEN_WORDS_PART_5)),
            delay(4000),
            tap(() => this.updateSpokenWordsContent(SPOKEN_WORDS_PART_6)),
            delay(4000),
            tap(() => this.updateSpokenWordsContent(SPOKEN_WORDS_PART_7)),
            delay(4000),
            tap(() => this.updateSpokenWordsContent(SPOKEN_WORDS_PART_8)),
            delay(4000),
            tap(() => this.updateSpokenWordsContent(SPOKEN_WORDS_PART_9)),
            delay(4000),
            tap(() => this.updateSpokenWordsContent(SPOKEN_WORDS_PART_10)),
            delay(4000),
            tap(() => this.updateSpokenWordsContent(SPOKEN_WORDS_PART_11)),
            delay(1800),
            tap(() => this.showReadBack = true),
        ).subscribe();
    }

    private startEditorPlayscript() {
        timer(3000)
        .pipe(
            tap(() => this.updateEditorContent('', EDITOR_PART_1)),
            delay(4000),
            tap(() => this.updateEditorContent(EDITOR_PART_1_NO_CURSOR, EDITOR_PART_2)),
            delay(4000),
            tap(() => this.updateEditorContent(EDITOR_PART_1_NO_CURSOR, '')),
            delay(4000),
            tap(() => this.updateEditorContent(EDITOR_PART_1_NO_CURSOR, EDITOR_PART_3)),
            delay(4000),
            tap(() => this.updateEditorContent(EDITOR_PART_3_NO_CURSOR, EDITOR_PART_4)),
            delay(4000),
            tap(() => this.updateEditorContent(EDITOR_PART_5, '')),
            delay(4000),
            tap(() => this.updateEditorContent(EDITOR_PART_6, '')),
            delay(4000),
            tap(() => {
                this.showMockSynonymPopover = true;
                this.updateEditorContent(EDITOR_PART_7, '')
            }),
            delay(4000),
            tap(() => {
                this.showMockSynonymPopover = false;
                this.updateEditorContent(EDITOR_PART_8, '')
            }),
            delay(4000),
            tap(() => {
                this.updateEditorContent(EDITOR_PART_9, '')
            }),
        ).subscribe();
    }
    
    private updateEditorContent(nonAnimated: string, animated: string) {
        this.nonAnimatedEditorContent = nonAnimated;
        this.animatedEditorContent = this.sanitizer.bypassSecurityTrustHtml(animated);
        this.triggerEditorAnimation();
    }

    private updateSpokenWordsContent(spokenWords: string) {
        this.spokenWords = spokenWords;
        this.triggerSpokenWordsAnimation();
    }

    private triggerEditorAnimation() {
        this.showAnimatedEditorText = false;
        setTimeout(() => this.showAnimatedEditorText = true, 1);
    }

    private triggerSpokenWordsAnimation() {
        this.showAnimatedSpokenWords = false;
        setTimeout(() => this.showAnimatedSpokenWords = true, 1);
    }
}
