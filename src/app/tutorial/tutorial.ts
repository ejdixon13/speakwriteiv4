import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../services/user.service';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';


@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
  styleUrls: ['./tutorial.scss']
})
export class TutorialPage implements OnInit {

  constructor(
    public router: Router,
    private userService: UserService) { }
  
  slideTwoStarted: boolean = false;
  @ViewChild('slides', {static: false})
  slides: IonSlides;;

  completeTutorial() {
    this.userService.setAppBeenOpenedPreviously(true)
      .then(() => this.router.navigate(['/home']));
  }

  afterSlideChange(slides) {
    this.slides.getActiveIndex().then(activeIndex => {
      this.slideTwoStarted = activeIndex === 1;
    });
  }


  ngOnInit() {
  }


}
