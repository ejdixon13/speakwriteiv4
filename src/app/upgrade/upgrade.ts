import { Component } from '@angular/core';
import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2/ngx';
import { ManagedProduct } from '../app.model';

@Component({
  selector: 'page-upgrade',
  templateUrl: 'upgrade.html',
  styleUrls: ['./upgrade.scss']
})
export class UpgradePage {
    constructor(private store: InAppPurchase2 ) {}
    upgrade() {
        this.store.order(ManagedProduct.SPEAKWRITE_PREMIUM);
    }
}
