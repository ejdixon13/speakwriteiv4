import { WordPunct, Sentence, VALID_SENTENCE_END, VALID_SENTENCE_END_REG_EX } from '../../models/grammar.model';
import { words } from 'lodash';

const WORD_PUNCT_REG_EX = /[\w']+|["<>/;:=+)(&$#.,!?;]/g;
const WORD_CHAR_REG_EX = /[\w']+/g;
const SPACE_REG_EX = /\s/g;
const LONGEST_WORD_LENGTH = 50;

const isWordChar = (char: string) => char.match(WORD_CHAR_REG_EX) !== null;
const isSpace = (char: string) => char.match(SPACE_REG_EX) !== null;
const getCharAtIndex = (index: number, text: string) => text.substr(index, 1);
const isBeginningOfDoc = (index: number): boolean => index === 0;
const isEndOfDoc = (index: number, text: string): boolean => index === text.length;
const isPastBeginningOfDoc = (index: number): boolean => index < 0;
const areIndexAndLengthTheSame =
    (wordPunct1: WordPunct, wordPunct2) =>
        (wordPunct1.position.index === wordPunct2.position.index) && (wordPunct1.position.length === wordPunct2.position.length);
const areSentenceIndexAndLengthTheSame =
    (sentence1: Sentence, sentence2: Sentence) =>
        (sentence1.range.index === sentence2.range.index) && (sentence1.range.length === sentence2.range.length);
const isValidSentenceEnd = (char: string) => char.match(VALID_SENTENCE_END_REG_EX) !== null;


export function getWordPunctAtIndex(index: number, text: string): WordPunct {
    let wordPunct = new WordPunct(null, null);
    

    while (wordPunct.value === null) {
        index--;
        if (isPastBeginningOfDoc(index)) {
            wordPunct = new WordPunct('', { index: 0, length: 0 } );
        } else {
            const charAtIndex = getCharAtIndex(index, text);
            if (isWordChar(charAtIndex)) {
                wordPunct = getWord(index, text);
            } else if (!isSpace(charAtIndex)) {
                // assume punctuation
                wordPunct = new WordPunct(charAtIndex, { index: index, length: 1 });
            }
        }
    }
    return wordPunct;
}

/**
 * This function assumes substr(index, 1) is a word character.
 *  To get current word (if the cursor position is a letter), iterate backwards until space or punctuation hit. 
 *  use lodash words with longest word length and grab first word in array
 * @param index
 * @param text
 */
function getWord(index: number, text: string): WordPunct {
    const currentWordCharExists = isWordChar(text.substr(index, 1));
    if (!currentWordCharExists) {
        throw Error('No starting letter present');
    }

    let wordPunct = new WordPunct(null, null);

    while (wordPunct.value === null) {
        index--;
        if (isPastBeginningOfDoc(index)) {
            const wordValue = words(text, WORD_PUNCT_REG_EX)[0];
            wordPunct = new WordPunct(wordValue, { index: 0, length: wordValue.length } );
        } else {
            const charAtIndex = getCharAtIndex(index, text);
            if (!isWordChar(charAtIndex)) {
                const firstLetterOfWordIndex = index + 1;
                const textToGetWordFrom = text.substr(firstLetterOfWordIndex, LONGEST_WORD_LENGTH);
                const wordValue = words(textToGetWordFrom, WORD_PUNCT_REG_EX)[0];
                wordPunct = new WordPunct(wordValue, { index: firstLetterOfWordIndex, length: wordValue.length } );
            }
        }
    }
    return wordPunct;
}

export function getPriorWordPunct(index: number, text: string): WordPunct {
    const startingWord = getWordPunctAtIndex(index, text);
    let priorWord = startingWord;

    while (startingWord.position.index === priorWord.position.index) {
        priorWord = getWordPunctAtIndex(priorWord.position.index, text);
        if (isBeginningOfDoc(priorWord.position.index)) {
            return priorWord;
        }
    }
    return priorWord;
}

export function getNextWordPunct(index: number, text: string): WordPunct {
    const startingWord = getWordPunctAtIndex(index, text);
    let nextWord = startingWord;
    let nexWordPointer = nextWord.position.index + nextWord.position.length;

    while (areIndexAndLengthTheSame(startingWord, nextWord)) {
        if (isEndOfDoc(nexWordPointer, text)) {
            return nextWord;
        }
        nexWordPointer++;
        nextWord = getWordPunctAtIndex(nexWordPointer, text);
    }
    return nextWord;

}

  /**
   * if first non-space char is a sentence end, continue until next sentence end, otherwise go to first sentence end
   *   examples:
   *   BEG OF DOC:  Hello How are you? {cursor} END OF DOC = "Hello How are you?"
   *   end of sentence. Hello How are you? {cursor}More words... = "Hello How are you?"
   *   BEG OF DOC:{cursor}Hello How are you? = ""
   *   Hello How are{cursor} you END OF DOC = "Hello How are you"
   *   Another sentence. Hello How are yo{cursor}u?  = "Hello How are you?"
   **/
export function getSentenceAtIndex(index: number, text: string): Sentence {
    let sentenceEndIndex = null;
    let setenceBeginIndex = null;
    const wordPunctAtIndex = getWordPunctAtIndex(index, text);
    if (wordPunctAtIndex.value === '') {
        return new Sentence('', { index: 0, length: 0 });
    }

    // if wordpunct valid sentence end continue until next sentence end or begininng of doc
    setenceBeginIndex = getIndexOfSentenceBeginning(wordPunctAtIndex, text);
    sentenceEndIndex = getIndexOfSentenceEnd(wordPunctAtIndex, text);
    const sentenceLength = sentenceEndIndex - setenceBeginIndex;
    return new Sentence(text.substr(setenceBeginIndex, sentenceLength), { index: setenceBeginIndex, length: sentenceLength });

}

export function getNextSentence(index: number, text: string): Sentence {
    const startingSentence = getSentenceAtIndex(index, text);
    let nextSentence = startingSentence;
    let nextSentencePointer = nextSentence.range.index + nextSentence.range.length;

    while (areSentenceIndexAndLengthTheSame(startingSentence, nextSentence)) {
        if (isEndOfDoc(nextSentencePointer, text)) {
            return nextSentence;
        }
        nextSentencePointer++;
        nextSentence = getSentenceAtIndex(nextSentencePointer, text);
    }
    return nextSentence;
}

function getIndexOfSentenceEnd(wordPunct: WordPunct, text: string) {
    let sentenceEndWordPunct = { ...wordPunct } as WordPunct;
    if (!isValidSentenceEnd(sentenceEndWordPunct.value)) {
        while (true) {
            const nextWordPunct =
                getNextWordPunct(sentenceEndWordPunct.position.index + sentenceEndWordPunct.position.length, text);
            const endOfDoc = sentenceEndWordPunct.position.index === nextWordPunct.position.index;
            if (endOfDoc || isValidSentenceEnd(sentenceEndWordPunct.value)) {
                break;
            }
            sentenceEndWordPunct = nextWordPunct;
        }
    }

    return isValidSentenceEnd(sentenceEndWordPunct.value) ?
        getIndexOfSentenceEndIfAtSentenceEnd(sentenceEndWordPunct, text) :
        (sentenceEndWordPunct.position.index + sentenceEndWordPunct.position.length);
}

function getIndexOfSentenceEndIfAtSentenceEnd(wordPunct: WordPunct, text: string) {
    let sentenceEndWordPunct = {...wordPunct} as WordPunct;
    let sentenceEndIndex = null;

    if (!isValidSentenceEnd(sentenceEndWordPunct.value)) {
        throw Error('Must be sentence end! Actual wordPunct: ' +  sentenceEndWordPunct.value);
    }
    while (sentenceEndIndex === null) {
        const nextWordPunct = getNextWordPunct(sentenceEndWordPunct.position.index + sentenceEndWordPunct.position.length, text);
        const endOfDoc = sentenceEndWordPunct.position.index === nextWordPunct.position.index;
        if (endOfDoc || !isValidSentenceEnd(nextWordPunct.value)) {
            sentenceEndIndex = sentenceEndWordPunct.position.index + sentenceEndWordPunct.position.length;
        }
        sentenceEndWordPunct = nextWordPunct;
    }

    return sentenceEndIndex;
}

function getIndexOfSentenceBeginning(currentWordPunct: WordPunct, text: string) {
    let sentenceBegWordPunct = { ...currentWordPunct } as WordPunct;
    const begOfDoc = (wordPunct: WordPunct) => (wordPunct.position.index === 0) && (wordPunct.position.length === 0);

    // ensure multiple sentence ends in a row are accounted for
    while (true) {
        const priorWordPunct = getWordPunctAtIndex(sentenceBegWordPunct.position.index, text);
        if (!isValidSentenceEnd(priorWordPunct.value)) {
            break;
        }
        sentenceBegWordPunct = getWordPunctAtIndex(sentenceBegWordPunct.position.index, text);
    }

    if (!begOfDoc(getWordPunctAtIndex(sentenceBegWordPunct.position.index, text))) {
        while (true) {
            const priorWordPunct = getWordPunctAtIndex(sentenceBegWordPunct.position.index, text);
            if (isValidSentenceEnd(priorWordPunct.value) || begOfDoc(priorWordPunct)) {
                break;
            }

            sentenceBegWordPunct = getWordPunctAtIndex(sentenceBegWordPunct.position.index, text);
        }
    }

    return sentenceBegWordPunct.position.index;
}

