import { RangeStatic, Quill } from 'quill';
import { words } from 'lodash';
import { getWordPunctAtIndex, getPriorWordPunct, getNextWordPunct, getSentenceAtIndex } from './word-punct.util';
  
  
// cursor position =  index:0, length:-1'
describe('word-punct.util', () => {
    let quillEditor: Quill;
    const str = '"Hi   susan" & I thought better, "Hi Jack".?';
    const openEndedString = '  "Hi!  ';
    const emptyString = '  ';
  
    beforeEach(() => {
    })

    it('should set index to the end of wordPunct after wordPunct selection', () => {
    });


    describe('wordPunctAtIndex', () => {
        it('should get empty string and set position to 0 if cursor position is beginning of the doc', () => {
            const wordPunctAtIndex = getWordPunctAtIndex(0, str);

            expect(wordPunctAtIndex.value).toBe("");
            expect(wordPunctAtIndex.position.index).toBe(0);
            expect(wordPunctAtIndex.position.length).toBe(0);
        });
        it('should get prior wordPunct if cursor position is a space', () => {
            let wordPunctAtIndex = getWordPunctAtIndex(5, str);
            
            expect(wordPunctAtIndex.value).toBe("Hi");
            expect(wordPunctAtIndex.position.index).toBe(1);
            expect(wordPunctAtIndex.position.length).toBe(2);
            
            wordPunctAtIndex = getWordPunctAtIndex(13, str);
            expect(wordPunctAtIndex.value).toBe("\"");
            expect(wordPunctAtIndex.position.index).toBe(11);
            expect(wordPunctAtIndex.position.length).toBe(1);
        });
        it('should get empty string and set position to 0 if cursor position is a space and there are only spaces until beginning of doc', () => {
            let wordPunctAtIndex = getWordPunctAtIndex(2, emptyString);
            
            expect(wordPunctAtIndex.value).toBe("");
            expect(wordPunctAtIndex.position.index).toBe(0);
            expect(wordPunctAtIndex.position.length).toBe(0);
        });
        it('should get current wordPunct if cursor position is a punctuation', () => {
            const wordPunctAtIndex = getWordPunctAtIndex(1, str);

            expect(wordPunctAtIndex.value).toBe("\"");
            expect(wordPunctAtIndex.position.index).toBe(0);
            expect(wordPunctAtIndex.position.length).toBe(1);
        });
        it('should get current wordPunct if cursor position is a letter', () => {
            const wordPunctAtIndex = getWordPunctAtIndex(8, str);

            expect(wordPunctAtIndex.value).toBe("susan");
            expect(wordPunctAtIndex.position.index).toBe(6);
            expect(wordPunctAtIndex.position.length).toBe(5);
        });
        it('should get standalone punctuation if cursor position is space before standalone punctuation', () => {
            const wordPunctAtIndex = getWordPunctAtIndex(15, str);

            expect(wordPunctAtIndex.value).toBe("&");
            expect(wordPunctAtIndex.position.index).toBe(13);
            expect(wordPunctAtIndex.position.length).toBe(1);
        });
       
    })

    // to get prior wordPunct, just set index to beginning of wordPunct and getCurrentWordPunct
    describe('prior wordPunct', () => {
        it('should get empty string and set position to 0 if cursor position is beginning of the doc', () => {
            const wordPunctAtIndex = getPriorWordPunct(0, str);

            expect(wordPunctAtIndex.value).toBe("");
            expect(wordPunctAtIndex.position.index).toBe(0);
            expect(wordPunctAtIndex.position.length).toBe(0);
        });
        it('should get empty string and set position to 0 if word before current is beginning of the doc', () => {
            const wordPunctAtIndex = getPriorWordPunct(1, str);

            expect(wordPunctAtIndex.value).toBe("");
            expect(wordPunctAtIndex.position.index).toBe(0);
            expect(wordPunctAtIndex.position.length).toBe(0);
        });

        // This is because the highlighted word should be the word before the space
        it('should go back two wordPuncts and return that one if cursor position is a space', () => {
            const wordPunctAtIndex = getPriorWordPunct(4, str);

            expect(wordPunctAtIndex.value).toBe("\"");
            expect(wordPunctAtIndex.position.index).toBe(0);
            expect(wordPunctAtIndex.position.length).toBe(1);
        });
        it('should get prior wordPunct if cursor position is a punctuation', () => {
            const wordPunctAtIndex = getPriorWordPunct(14, str);

            expect(wordPunctAtIndex.value).toBe("\"");
            expect(wordPunctAtIndex.position.index).toBe(11);
            expect(wordPunctAtIndex.position.length).toBe(1);
        });
        it('should get prior wordPunct if cursor position is a letter', () => {
            const wordPunctAtIndex = getPriorWordPunct(8, str);

            expect(wordPunctAtIndex.value).toBe("Hi");
            expect(wordPunctAtIndex.position.index).toBe(1);
            expect(wordPunctAtIndex.position.length).toBe(2);
        });
    })

    // to get next wordPunct, set point to end of current wordPunct. Increase by 1, check current wordPunct. 
    // Repeat this process until current wordPunct is differs from original or end of doc. If end of doc use current wordPunct 
    describe('next wordPunct', () => {
        it('should get first wordPunct if cursor position is beginning of the doc', () => {
            const wordPunctAtIndex = getNextWordPunct(0, str);

            expect(wordPunctAtIndex.value).toBe("\"");
            expect(wordPunctAtIndex.position.index).toBe(0);
            expect(wordPunctAtIndex.position.length).toBe(1);
        });
        it('should get current wordPunct if cursor position is end of doc', () => {
            const wordPunctAtIndex = getNextWordPunct(str.length, str);

            expect(wordPunctAtIndex.value).toBe("?");
            expect(wordPunctAtIndex.position.index).toBe(43);
            expect(wordPunctAtIndex.position.length).toBe(1);
        });
        it('should get current wordPunct if cursor position is space before end of doc', () => {
            const wordPunctAtIndex = getNextWordPunct(7, openEndedString);

            expect(wordPunctAtIndex.value).toBe("!");
            expect(wordPunctAtIndex.position.index).toBe(5);
            expect(wordPunctAtIndex.position.length).toBe(1);
        });
        it('should get next wordPunct if cursor position is a space', () => {
            const wordPunctAtIndex = getNextWordPunct(13, str);

            expect(wordPunctAtIndex.value).toBe("&");
            expect(wordPunctAtIndex.position.index).toBe(13);
            expect(wordPunctAtIndex.position.length).toBe(1);
        });
        it('should get next wordPunct if cursor position is a punctuation', () => {
            const wordPunctAtIndex = getNextWordPunct(14, str);

            expect(wordPunctAtIndex.value).toBe("I");
            expect(wordPunctAtIndex.position.index).toBe(15);
            expect(wordPunctAtIndex.position.length).toBe(1);
        });
        it('should get next wordPunct if cursor position is a letter', () => {
            const wordPunctAtIndex = getNextWordPunct(2, str);

            expect(wordPunctAtIndex.value).toBe("susan");
            expect(wordPunctAtIndex.position.index).toBe(6);
            expect(wordPunctAtIndex.position.length).toBe(5);
        });
        
    })

    describe('getSentenceAtIndex', () => {
        const sentenceStr = 'Hi there. A sentence! Another sentence'
        const sentenceStr2 = 'Hi there... A & sentence'
        const sentenceStr3 = '  Hi there..'
        it('should return sentence before cursor position if cursor position is space after sentence', () => {
            const sentenceAtIndex = getSentenceAtIndex(22, sentenceStr);

            expect(sentenceAtIndex.value).toBe('A sentence!');
            expect(sentenceAtIndex.range.index).toBe(10);
            expect(sentenceAtIndex.range.length).toBe(11);
        })
        it('should return sentence before cursor position if cursor position is space after sentence, and before sentence is beginning of doc', () => {
            const sentenceAtIndex = getSentenceAtIndex(10, sentenceStr);

            expect(sentenceAtIndex.value).toBe('Hi there.');
            expect(sentenceAtIndex.range.index).toBe(0);
            expect(sentenceAtIndex.range.length).toBe(9);
        })
        it('should return empty string and index/length as 0 if at beginning of doc', () => {
            const sentenceAtIndex = getSentenceAtIndex(0, sentenceStr);

            expect(sentenceAtIndex.value).toBe('');
            expect(sentenceAtIndex.range.index).toBe(0);
            expect(sentenceAtIndex.range.length).toBe(0);
        })
        it('should return sentence where cursor position is, if cursor position is in middle of sentence', () => {
            const sentenceAtIndex = getSentenceAtIndex(0, sentenceStr);

            expect(sentenceAtIndex.value).toBe('');
            expect(sentenceAtIndex.range.index).toBe(0);
            expect(sentenceAtIndex.range.length).toBe(0);
        })
        it('should return sentence without sentence end if at End of doc', () => {
            const sentenceAtIndex = getSentenceAtIndex(24, sentenceStr2);

            expect(sentenceAtIndex.value).toBe('A & sentence');
            expect(sentenceAtIndex.range.index).toBe(12);
            expect(sentenceAtIndex.range.length).toBe(12);
        })
        it('should ignore multiple sentence end puntuation in a row', () => {
            let sentenceAtIndex = getSentenceAtIndex(12, sentenceStr2);

            expect(sentenceAtIndex.value).toBe('Hi there...');
            expect(sentenceAtIndex.range.index).toBe(0);
            expect(sentenceAtIndex.range.length).toBe(11);

            sentenceAtIndex = getSentenceAtIndex(1, sentenceStr2);

            expect(sentenceAtIndex.value).toBe('Hi there...');
            expect(sentenceAtIndex.range.index).toBe(0);
            expect(sentenceAtIndex.range.length).toBe(11);
            
        })
        it('should return empty string if cursor position is on space, and there are only spaces until beginning of doc', () => {
            const sentenceAtIndex = getSentenceAtIndex(2, sentenceStr3);

            expect(sentenceAtIndex.value).toBe('');
            expect(sentenceAtIndex.range.index).toBe(0);
            expect(sentenceAtIndex.range.length).toBe(0);
            
        })
    })

})
