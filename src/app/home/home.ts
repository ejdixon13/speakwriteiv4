import { Component, Inject, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { filter, get, last } from 'lodash';
import { FolderModel } from 'src/models/folders.model';
import { DocumentService } from '../services/document.service';
import { FolderService } from '../services/folder.service';
import { EditorPage } from '../editor/editor';
import { CardPopover } from '../popovers/card-popover';
import { NavController, PopoverController, AlertController, IonFab } from '@ionic/angular';
import { DocumentModel } from 'src/models/documents.model';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  styleUrls: ['./home.scss']
})
export class HomePage implements OnInit, OnDestroy {

  documents: Array<DocumentModel>;
  folderStack: Array<FolderModel> = [];
  refreshDocsSubscription: Subscription;
  showItems: boolean = false;
  loadingDocuments: boolean = false;

  @ViewChild('fab', {static: false})
  fab: IonFab;
  
  quillForm = this.formBuilder.group({
    quillEditorDelta: null
  });

  constructor(
    public router: Router,
    private docService: DocumentService,
    private folderService: FolderService,
    private popoverCtrl: PopoverController,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController
  ) {

    this.refreshDocsSubscription = docService.docsRefreshed$.subscribe(
      (documents: DocumentModel[]) => {
        this.documents = filter(documents, {folderId: this.currentFolder.id});
        this.showItems = true;
        this.loadingDocuments = false;
      });

  }

  get currentFolder() {
    return last(this.folderStack);
  }

  pushFolder(folder: FolderModel) {
    this.showItems = false;
    this.loadingDocuments = true;
    this.folderStack.push(folder);
    this.docService.refreshDocs();
  }

  popFolder() {
    if (this.folderStack.length > 1) {
      this.showItems = false;
      this.folderStack.pop();
      this.docService.refreshDocs();
    }
  }

  createNewDoc(event) {
    setTimeout(() =>  this.fab.close(), 1000);
    this.alertCtrl.create({
      header: 'New Document',
      inputs: [
        {
          name: 'title',
          placeholder: 'Title'
        }
      ],
      buttons: [
        {
          text: 'Create',
          handler: data => {
            // TODO: constrict title length
            // if(data.title.length > 30) {
            //   alert('Title too long');
            // }
            if (data.title) {
              this.docService.createNewDocument(data.title, this.currentFolder.id)
                .then((doc: DocumentModel) => {
                  this.docService.refreshDocs();
                  this.navigateToEditorPage(doc.id);
                });
            } else {
              return;
            }
          }
        }
      ]
    })
      .then(
        alert => alert.present()
      );

  }

  createNewFolder(event) {
    setTimeout(() =>  this.fab.close(), 1000);
    this.alertCtrl.create({
      header: 'New Folder',
      inputs: [
        {
          name: 'title',
          placeholder: 'Folder name'
        }
      ],
      buttons: [
        {
          text: 'Create',
          handler: data => {
            if (data.title) {
              this.folderService.createNewFolder(data.title, this.currentFolder.id)
                .then((newFolder: FolderModel) => this.currentFolder.folders.push(newFolder))
            } else {
              return;
            }
          }
        }
      ]
    })
      .then(alert => alert.present());
  }

  navigateToEditorPage(id: any) {
    // push another page onto the navigation stack
    // causing the nav controller to transition to the new page
    // optional data can also be passed to the pushed page.
    this.router.navigate(['/editor/', id]);
  }

  presentItemPopover(myEvent: Event, itemType, id, parentFolderId?: number) {
    myEvent.stopPropagation();

    this.popoverCtrl.create({
      component: CardPopover,
      componentProps: { id: id, parentFolderId: parentFolderId, type: itemType }
    })
      .then(
        popover => {
          popover.present();
          // popover.present({
          //   ev: myEvent
          // });

          popover.onDidDismiss()
            .then((details) => {
              if (get(details.data, 'folderDeleted')) {
                this.folderService.getRootFolder().then((rootFolder) => {
                  const newFolderStack = [];
                  while (this.folderStack.length > 0) {
                    const folderToReplace = this.folderStack.pop();
                    const folderReplacement = this.folderService.getFolder(rootFolder, folderToReplace.id);
                    newFolderStack.push(folderReplacement);
                  }
                  this.folderStack = newFolderStack.reverse(); // reverse because they were inserted backwards
                });
              }
            });
        }
      );
    
  }

  ngOnInit() {
    this.folderService.getRootFolder().then((folder) => {
      this.folderStack.push(folder);
      this.docService.refreshDocs();
    });
  }

  ngOnDestroy() {
    this.refreshDocsSubscription.unsubscribe();
    // this.refreshFoldersSubscription.unsubscribe();
  }

}
