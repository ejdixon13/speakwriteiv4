import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2/ngx';
import { UserService } from './services/user.service';
import { PaidVsFreeService } from './services/paidVsFree.service';
import { TutorialPage } from './tutorial/tutorial';
import { timer } from 'rxjs';
import { Router } from '@angular/router';
import { ManagedProduct } from './app.model';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  showSplash = true;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private userService: UserService,
    private paidVsFreeService: PaidVsFreeService,
    private menuCtrl: MenuController,
    private router: Router,
    private store: InAppPurchase2
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
            // userService.setAppBeenOpenedPreviously(false); // TODO: remove when deploying to app store
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.userService.hasAppBeenOpenedPreviously()
        .then((hasAppBeenOpenedPreviously: boolean) => {
          if (!hasAppBeenOpenedPreviously) {
            this.router.navigate(['tutorial']);
            this.userService.setAppBeenOpenedPreviously(true);
            this.paidVsFreeService.setDefaultWordLimit();
          }
          this.statusBar.styleDefault();
          this.splashScreen.hide();

          timer(1000).subscribe(() => this.showSplash = false);
        });

      this.store.register({
        id: ManagedProduct.SPEAKWRITE_PREMIUM,
        type: this.store.CONSUMABLE,
      });
      this.store
        .when(ManagedProduct.SPEAKWRITE_PREMIUM)
        .approved(p => {
          this.unlockApp();
          p.finish();
        });
      this.store
        .when(ManagedProduct.SPEAKWRITE_PREMIUM)
        .owned(() => this.unlockApp());
      this.store.refresh();
    });
  }

  private unlockApp() {
    this.paidVsFreeService.setIsPaid(true);
  }

  openPage(route: string[]) {
    this.menuCtrl.close();
    this.router.navigate(route);
  }

  closeMenu() {
    this.menuCtrl.close();
  }
}
