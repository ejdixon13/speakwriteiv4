import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HomePage } from './home/home';
import { EditorPage } from './editor/editor';
import { TutorialPage } from './tutorial/tutorial';
import { CommandListPage } from './commandList/command-list';
import { SettingsPage } from './settings/settings';
import { AboutPage } from './about/about';
import { UpgradePage } from './upgrade/upgrade';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomePage },
  { path: 'tutorial', component: TutorialPage },
  { path: 'command-list', component: CommandListPage },
  { path: 'settings', component: SettingsPage },
  { path: 'upgrade', component: UpgradePage },
  { path: 'about', component: AboutPage },
  { path: 'editor/:id', component: EditorPage }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
