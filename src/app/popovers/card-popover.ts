import { Component } from '@angular/core';
import { NavParams, AlertController, PopoverController } from '@ionic/angular';
import { get } from 'lodash';
import { DocumentService } from '../services/document.service';
import { FolderService } from '../services/folder.service';


@Component({
  template: `
    <ion-list lines="none">
      <ion-list-header> <ion-label>Actions</ion-label></ion-list-header>
      <ion-item (click)="deleteAction()">
        <ion-label>
          Delete
        </ion-label>
    </ion-item>
    </ion-list>
  `
})
// tslint:disable-next-line:component-class-suffix
export class CardPopover {
  type: string;
  id: string;
  parentFolderId: string;
  constructor(public navParams: NavParams,
              public folderService: FolderService,
              private alertCtrl: AlertController,
              private popoverCtrl: PopoverController,
              public docService: DocumentService) {}

  deleteAction() {
    // DELETE DOC
    if (this.type === 'doc') {
      this.showDeleteConfirmation(
        () => this.docService
          .deleteDocument(this.navParams.data.id)
          .then(() => {
            this.docService.refreshDocs();
            this.alertCtrl.dismiss({});
            this.popoverCtrl.dismiss({});
          })
      );
      
    } else if (this.type === 'folder'
      && this.id
      && this.parentFolderId) {
      this.showDeleteConfirmation(
        () => this.folderService
          .deleteFolder(this.id, this.parentFolderId)
          .then(() => {
            this.docService.refreshDocs();
            this.alertCtrl.dismiss({ folderDeleted: true });
            this.popoverCtrl.dismiss({ folderDeleted: true });
          })
      );
    }
  }

  showDeleteConfirmation(onConfirmAction: () => void) {
    this.alertCtrl.create({
      header: 'Are you sure you want to delete this item?',
      message: '(This item cannot be recovered)',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('No clicked');
            
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Yes clicked');
            onConfirmAction();
          }
        }
      ]
    })
      .then( alert => alert.present());
  }
}
