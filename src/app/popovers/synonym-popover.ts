import { Component, OnInit, OnDestroy } from '@angular/core';
import { ThesaurusService } from '../services/thesaurus.service';
import { PaidVsFreeService } from '../services/paidVsFree.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { QuillEditorService } from '../components/quill-editor/quill-editor.service';
import { PopoverController } from '@ionic/angular';

const PAGE_SIZE = 15;
@Component({
    template: `
    <div class="synonym-popover">
        <div class="synonym-header">Synonyms</div>
        <div class="synonym-popover-link-top" *ngIf="canShowPageUpArrow">
            <button (click)="showPrevPage()" class="ws-icon ws-icon--arrow-up-circle ws-icon--small"></button>
        </div>
        <div class="synonym-word-container">
            <div class="synonym-word" 
                 *ngFor="let synoym of synonymsShown; let i = index"
                 [ngClass]="{'synonym-word--blur' : isWordBlurred(i)}">
                {{synoym}}
            </div>
        </div>
        <div class="synonym-popover-link-bottom" *ngIf="canShowPageDownArrow">
            <button (click)="showNextPage()" class="ws-icon ws-icon--arrow-down-circle ws-icon--small"></button>
        </div>
        <ion-button *ngIf="!isPaid" expand="block" fill="outline" color="tertiary" (click)="goToUpgradePage()">
            <span class="synonym-popover__upgrade-icon"></span>
            <strong>Upgrade to see all results</strong>
        </ion-button>
    </div>
    `,
    styleUrls: ['synonym-popover.scss']
})

export class SynonymPopoverComponent implements OnInit, OnDestroy {
    fullSynonymList: string[] = [];
    synonymsShown: string[] = [];
    greaterThanOnePage = false;
    isShowMore = false;
    index = 0;
    page = 0;
    isPaid: boolean = false;

    synonymsRefreshedSubscription: Subscription;
    isPaidSubscription: Subscription;

    constructor(
        public thesaurausService: ThesaurusService,
        private paidVsFreeService: PaidVsFreeService,
        private popoverController: PopoverController,
        private router: Router,) { }

    getPage(page: number) {
        const start = page * PAGE_SIZE;
        const end = start + PAGE_SIZE;
        return this.fullSynonymList.slice(start, end);
    }

    goToUpgradePage() {
        this.popoverController.dismiss();
        this.router.navigate(['/upgrade']);
    }
    
    get canShowPageDownArrow() {
        const moreThanOnePage = this.fullSynonymList.length > PAGE_SIZE;
        const notLastPage = ((this.page + 1) * PAGE_SIZE) <= this.fullSynonymList.length;
        return moreThanOnePage && notLastPage && this.isPaid;
    }

    get canShowPageUpArrow() {;
        return this.page !== 0 && this.isPaid;
    }

    isWordBlurred(index: number) {
        return (index !== 0) && !this.isPaid;
    }

    showNextPage() {
        this.synonymsShown = this.getPage(++this.page);
    }

    showPrevPage() {
        this.synonymsShown = this.getPage(--this.page);
    }

    ngOnInit() {
        this.synonymsRefreshedSubscription =
            this.thesaurausService.synonymsRefreshed$
            .subscribe(synonyms => {
                this.fullSynonymList = synonyms;
                this.synonymsShown = this.getPage(0);
            });
        this.isPaidSubscription = this.paidVsFreeService.isPaid$
            .subscribe(isPaid => this.isPaid = isPaid)
    }
    
    ngOnDestroy() {
        this.synonymsRefreshedSubscription.unsubscribe();
        this.isPaidSubscription.unsubscribe();
    }
}
