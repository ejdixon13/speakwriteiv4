import { Component, OnInit } from '@angular/core';
import { ThesaurusService } from '../services/thesaurus.service';
import { PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { PaidVsFreeService } from '../services/paidVsFree.service';
import { Subscription } from 'rxjs';

const PAGE_SIZE = 3;
@Component({
    template: `
    <div class="definition-popover">
        <div class="definition-header">Definition</div>
        <div class="definition-popover-link-top" *ngIf="canShowPageUpArrow">
            <button (click)="showPrevPage()" class="ws-icon ws-icon--arrow-up-circle ws-icon--small"></button>
        </div>
        <div class="definition-container">
            <div class="definition-text"
            [ngClass]="{'definition-text--blur' : isDefinitionBlurred()}"
             *ngFor="let definition of definitionsShown">{{definition}}</div>
        </div>
        <div class="definition-popover-link-bottom" *ngIf="canShowPageDownArrow">
            <button (click)="showNextPage()" class="ws-icon ws-icon--arrow-down-circle ws-icon--small"></button>
        </div>
        <ion-button *ngIf="!isPaid" expand="block" fill="outline" color="tertiary" (click)="goToUpgradePage()">
            <span class="definition-popover__upgrade-icon"></span>
            <strong>Upgrade to see results</strong>
        </ion-button>
    </div>
    `,
    styleUrls: ['definition-popover.scss']
})

export class DefinitionPopoverComponent implements OnInit {
    fullDefinitionList: string[] = [];
    definitionsShown: string[] = [];
    greaterThanOnePage = false;
    isShowMore = false;
    index = 0;
    page = 0;
    isPaid: boolean = false;

    synonymsRefreshedSubscription: Subscription;
    isPaidSubscription: Subscription;

    constructor(
        public thesaurausService: ThesaurusService,
        private paidVsFreeService: PaidVsFreeService,
        private popoverController: PopoverController,
        private router: Router) { }

    getPage(page: number) {
        const start = page * PAGE_SIZE;
        const end = start + PAGE_SIZE;
        return this.fullDefinitionList.slice(start, end);
    }

    goToUpgradePage() {
        this.popoverController.dismiss();
        this.router.navigate(['/upgrade']);
    }

    isDefinitionBlurred(index: number) {
        return !this.isPaid;
    }
    
    get canShowPageDownArrow() {
        const moreThanOnePage = this.fullDefinitionList.length > PAGE_SIZE;
        const notLastPage = ((this.page + 1) * PAGE_SIZE) <= this.fullDefinitionList.length;
        return moreThanOnePage && notLastPage && this.isPaid;
    }

    get canShowPageUpArrow() {;
        return (this.page !== 0) && this.isPaid;
    }

    showNextPage() {
        this.definitionsShown = this.getPage(++this.page);
    }

    showPrevPage() {
        this.definitionsShown = this.getPage(--this.page);
    }

    ngOnInit() {
        this.synonymsRefreshedSubscription = this.thesaurausService.synonymsRefreshed$
            .subscribe(synonyms => {
                this.fullDefinitionList = synonyms;
                this.definitionsShown = this.getPage(0);
            });
        
        this.isPaidSubscription = this.paidVsFreeService
            .isPaid$
            .subscribe(isPaid => this.isPaid = isPaid);
    }
    
    ngOnDestroy() {
        this.synonymsRefreshedSubscription.unsubscribe();
        this.isPaidSubscription.unsubscribe();
    }
}
