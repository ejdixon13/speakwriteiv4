import { NgModule } from '@angular/core';

import { QuillEditorComponent } from './quillEditor.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    QuillEditorComponent
  ],
  exports: [
    QuillEditorComponent
  ]
})
export class QuillEditorModule {}
