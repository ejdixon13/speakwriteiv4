import { Injectable } from '@angular/core';
import { includes, lowerFirst, upperFirst } from 'lodash';
import * as QuillNamespace from 'quill';
import { RangeStatic } from 'quill';
import { Subject } from 'rxjs';
import { Paragraph, PUNCTUATION, Sentence, VALID_SENTENCE_END, WordPunct } from '../../../models/grammar.model';
import { PaidVsFreeService } from '../../services/paidVsFree.service';
import { getWordPunctAtIndex, getPriorWordPunct, getNextWordPunct, getSentenceAtIndex, getNextSentence } from '../../util/word-punct.util';
import { take } from 'rxjs/operators';
import { PopoverController } from '@ionic/angular';
import { SynonymPopoverComponent } from 'src/app/popovers/synonym-popover';
import { ThesaurusService } from 'src/app/services/thesaurus.service';
import { DefinitionPopoverComponent } from 'src/app/popovers/definition-popover';
const TAG: string = 'JARVIS APP';
const Quill: any = QuillNamespace;
const NEW_LINE_PLACEHOLDER_CHAR = '_';

const LONGEST_WORD_LENGTH = 20;

@Injectable()
export class QuillEditorService {
  currentEditor: QuillNamespace.Quill;
  lastAddedPhrase: Sentence;
  lastAddedPhraseIsOpenQuote: boolean = false;
  private editorScrollSource = new Subject<any>();
  editorScrollMethodCalled$ = this.editorScrollSource.asObservable();

  private synonymPopover;
  private definitionPopover;

  constructor(
    private paidVsFreeService: PaidVsFreeService,
    private popoverController: PopoverController,
    private thesaurusService: ThesaurusService) { }
  

  updateHighlightedWord() {
    const quillEditor = this.getCurrentEditor();

    // let wordAtCursorPos: Word = this.getWordAtCursorPositionIgnorePunctuation();
    const wordAtCursorPos: WordPunct = this.getWordPunctAtPosition();
    quillEditor.removeFormat(0, quillEditor.getLength() - 1);
    quillEditor.formatText(wordAtCursorPos.position.index, wordAtCursorPos.position.length, 'background', 'pink');
    if (wordAtCursorPos.value === NEW_LINE_PLACEHOLDER_CHAR) {
      quillEditor.formatText(wordAtCursorPos.position.index, wordAtCursorPos.position.length, 'color', 'pink');
    }

    // TODO: remove temp code
    const selection = this.getSelection(false);
    quillEditor.formatText(selection.index, selection.length, 'background', 'blue');

  }

  getAllText() {
    return this.currentEditor.getText();
  }

  setCursorToEnd() {
    this.currentEditor.setSelection(this.currentEditor.getLength(), 0);
    const lastWord = this.getWordPunctAtPosition();
    if (lastWord) {
      this.setWordPunctSelection(lastWord);
    }
    this.updateHighlightedWord();
  }

  addWordsAtCursorPosition(words: string, isOpenQuote: boolean = false) {
    let formattedWords = words;

    // add words to count if free
    this.addToWordsUsed(words);

    const word = this.getWordPunctAtPosition();
    console.log('Word at Position: ' + word.value);

    // If new line or new paragraph add placeholder character
    if (this.isNewLineOrNewParagraph(words)) {
      formattedWords += NEW_LINE_PLACEHOLDER_CHAR;
    } else if (includes([...VALID_SENTENCE_END, '"'], word.value) || this.isBeginningOfDocument(word.position.index)) {
       // If valid sentence end, then uppercase first letter
      formattedWords = formattedWords.charAt(0).toUpperCase() + formattedWords.slice(1);
    }

    // If open qoute, add space before it
    if (isOpenQuote) {
      formattedWords = ' ' + formattedWords;
    }

    // check for punctuation, to add space
    if (!this.lastAddedPhraseIsOpenQuote) {
      formattedWords = includes([...VALID_SENTENCE_END, ...PUNCTUATION], formattedWords[0])
        ? formattedWords
        : ' ' + formattedWords;
    }

    const index1 = word.position.index + word.position.length;

    // save off last added phrase
    this.lastAddedPhrase = new Sentence(formattedWords, {
      index: index1,
      length: formattedWords.length
    });
    this.lastAddedPhraseIsOpenQuote = isOpenQuote;


    const cursor = this.getSelection(false);
    const content = this.currentEditor.getText(cursor.index - 1, 1);
    const isNewLineAtCursor = content === '\n';
    const isNewLinePlaceholderAtCursor = content === NEW_LINE_PLACEHOLDER_CHAR;

    if (isNewLineAtCursor || isNewLinePlaceholderAtCursor) {
      if (isNewLinePlaceholderAtCursor) {
        this.currentEditor.deleteText(cursor.index - 1, 1);
        this.currentEditor.insertText(cursor.index - 1, upperFirst(formattedWords.trim()));
      } else {
        this.currentEditor.insertText(cursor.index, formattedWords);
      }
    } else {
      const wordAtPosition = this.getWordPunctAtPosition(null, false);
      const index2 = wordAtPosition.position.index + wordAtPosition.position.length;
      this.currentEditor.insertText(index2, formattedWords);
      this.currentEditor.setSelection(index2 + formattedWords.length, 0);
    }
    this.scrollToCurrentWord(true);
  }

  // private isOpenQuote(str: string) {
  //   if (str !== '"') { return false; }
  //   const quotationCountInLastParagraph = this.getParagraphBeforePosition().value.match(/"/g).length;
  //   return (quotationCountInLastParagraph % 2) === 0;
  // }

  // add words to count if free
  private addToWordsUsed(words: string) {
    this.paidVsFreeService.isPaid$
      .pipe(take(1))
      .subscribe((isPaid) => {
        if (!isPaid) {
          this.paidVsFreeService.addToWordsUsed(words.split(' ').length);
        }
      })
    ;
  }

  private isNewLineAtCursor() {
    const cursor = this.getSelection(false);
    const content = this.currentEditor.getText(cursor.index - 1, 1);
    return content === '\n' || content === NEW_LINE_PLACEHOLDER_CHAR;
  }

  // Checks for new line or new paragraphs in order to see if a '_' should be added
  private isNewLineOrNewParagraph(word: string): boolean {
    return word === '\n' || word === '\n\n';
  }

  private isBeginningOfDocument(index: number) {
    while (!this.currentEditor.getText(index, 1).trim() && index > 0) {
      --index;
    }
    return index <= 0;
  }

  scrollToCurrentWord(smooth: boolean = false) {
    setTimeout(() => {
      const highlightedWord = document.querySelector('span[style*="background-color"]');
      if (highlightedWord) {
        highlightedWord
          .scrollIntoView({
            block: 'center',
            behavior: smooth ? 'smooth' : 'auto'
          });
      }
    }, 100);
  }

  getCurrentEditor(): QuillNamespace.Quill {
    return this.currentEditor;
  }

  setCurrentEditor(editor: QuillNamespace.Quill) {
    this.currentEditor = editor;
  }

  getWordPunctAtPosition(position?: RangeStatic, focus: boolean = true): WordPunct {
    if (!position) {
      position = this.getSelection(focus);
    }
    return getWordPunctAtIndex(position.index, this.currentEditor.getText());
  }

  private getSelection(focus: boolean = true): RangeStatic {
    let position: RangeStatic = this.currentEditor.getSelection(true);

    // try https://stackoverflow.com/questions/39359444/quill-js-unable-to-get-selection-range-on-a-readonlytrue-editor-in-quill-js-0
    if (!position) {
      position = (this.currentEditor as any).selection.savedRange;
    }
    console.log('Position Index: ' + position.index);
    console.log('Position Length: ' + position.length);

    return position;
  }

  goBackNumWordPuncts(numWords: number = 1) {
    const currentWordPunct = this.getWordPunctAtPosition();
    const docText = this.currentEditor.getText();
    let priorWordPunct = getPriorWordPunct(currentWordPunct.endIndex, docText);
    numWords--;

    while (numWords > 0) {
      priorWordPunct = getPriorWordPunct(priorWordPunct.endIndex, docText);
      numWords--;
    }
    this.setWordPunctSelection(priorWordPunct);
  }

  goForwardNumWordPuncts(numWords: number) {
    const currentWordPunct = this.getWordPunctAtPosition();
    const docText = this.currentEditor.getText();
    let nextWordPunct = getNextWordPunct(currentWordPunct.endIndex, docText);
    numWords--;

    while (numWords > 0) {
      nextWordPunct = getNextWordPunct(nextWordPunct.endIndex, docText);
      numWords--;
    }
    this.setWordPunctSelection(nextWordPunct);
  }

  goBackNumSentences(numSentences: number = 1) {
    const docText = this.currentEditor.getText();
    const currentSentence = this.getSentenceAtPosition();
    let priorSentence = getSentenceAtIndex(currentSentence.range.index, docText);
    numSentences--;

    while (numSentences > 0) {
      priorSentence = getSentenceAtIndex(priorSentence.range.index, docText);
      numSentences--;
    }
    const sentenceEndIndex = priorSentence.range.index + priorSentence.range.length;
    this.setWordPunctSelection(getWordPunctAtIndex(sentenceEndIndex, docText));
  }

  goForwardNumSentences(numSentences: number = 1) {
    let currentSentence = this.getSentenceAtPosition();
    let sentenceEndIndex = currentSentence.range.index + currentSentence.range.length;
    const docText = this.currentEditor.getText();

    numSentences = numSentences + 1; // TODO: This is a shim. Remove when you fix the real issue.

    while (numSentences > 0) {
      sentenceEndIndex = currentSentence.range.index + currentSentence.range.length;
      currentSentence = getNextSentence(sentenceEndIndex, docText);
      numSentences--;
    }
    this.setWordPunctSelection(getWordPunctAtIndex(sentenceEndIndex, docText));
  }

  goBackNumParagraphs(numWords: number = 1) {

  }

  goForwardNumParagraphs(numWords: number = 1) {
    
  }

  /**
   * Give some flexibility to the go to command to go to any word in the view
   */
  goToWordInLastFiveParagraphs(word: string = ''): boolean {
    if (word === '') {
      return false;
    }
    let count = 1;
    let paragraph = this.getParagraphBeforePosition();
    while (!this.goToWordInParagraph(word, paragraph) && count < 6) {
      count++;
      paragraph = this.getParagraphBeforePosition(paragraph.range);
    }
    if (count === 6) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * Give some flexibility to the go to command to go to any word in the view
   */
  goToWordInNextFiveParagraphs(word: string = ''): boolean {
    if (word === '') {
      return false;
    }
    let count = 1;
    const docText = this.currentEditor.getText();
    let paragraph = this.getParagraphBeforePosition();
    while (!this.goToWordInParagraph(word, paragraph) && count < 6) {
      count++;
      const endOfParagraph = paragraph.range.index + paragraph.range.length + 10;
      const sentence = getNextSentence(endOfParagraph, docText);
      sentence.range.index++;
      paragraph = this.getParagraphBeforePosition(sentence.range);
    }
    if (count === 6) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * returns true if word found, else false
   * @param word
   */
  goToWordInParagraph(word: string = '', paragraph: Paragraph): boolean {
    const docText = this.currentEditor.getText();
    const startOfParagraph = paragraph.range.index;
    const endOfParagraph = paragraph.range.index + paragraph.range.length;
    let index = endOfParagraph;
    while (index > startOfParagraph) {
      const wordPunct = getPriorWordPunct(index, docText);
      if (wordPunct.value.toLocaleLowerCase() === word.toLocaleLowerCase()) {
        this.setWordPunctSelection(wordPunct);
        this.updateHighlightedWord();
        this.scrollToCurrentWord(true);
        return true;
      }
      index = wordPunct.endIndex;
    }
    return false;
  }

  replaceWordWith(words: string = '') {
    this.deleteWordAtCusorPosition();
    this.addWordsAtCursorPosition(words);
  }

  setWordPunctSelection(wordPunct: WordPunct) {
    this.currentEditor.setSelection(wordPunct.position.index + wordPunct.position.length, 0);
  }

  getSentenceAtPosition(position: RangeStatic = this.getSelection()): Sentence {
    return getSentenceAtIndex(position.index, this.currentEditor.getText());
  }


  getParagraphBeforePosition(position: RangeStatic = this.getSelection()): Paragraph {
    const sentences: Sentence[] = [];
    // get first sentence
    const lastSentenceOfParagraph = this.getSentenceAtPosition(position);
    sentences.push(lastSentenceOfParagraph);
    // set paragraph end
    // get sentence range begin to input to while loop
    let rangePtr: RangeStatic = lastSentenceOfParagraph.range;

    // while not paragraph beginning
    while (!this.isParagraphBeginning(rangePtr.index - 3)) {
      sentences.unshift(this.getSentenceAtPosition(rangePtr));
      rangePtr = sentences[0].range;
    }

    const index = sentences[0].range.index;
    const length = lastSentenceOfParagraph.range.index + lastSentenceOfParagraph.range.length - index;
    return new Paragraph(sentences, { index: index, length: length });
  }

  // more than 3 spaces, or new line and not beginning of doc
  private isParagraphBeginning(index: number) {
    if (index < 3) {
      return true;
    }
    const spaceCheckStr = this.currentEditor.getText(index, 3);

    const containsTabOrNewLine = /[\t\r\n↵]/.test(spaceCheckStr);
    const isThreeSpaces = /^\s\s\s$/.test(spaceCheckStr);
    return containsTabOrNewLine || isThreeSpaces;
  }
  
  uncapitalizeWordAtCursorPosition() {
    const wordToUncapitalize = this.getWordPunctAtPosition();

    this.currentEditor.deleteText(wordToUncapitalize.position.index, wordToUncapitalize.position.length);
    this.currentEditor.insertText(wordToUncapitalize.position.index, lowerFirst(wordToUncapitalize.value));
  }
  capitalizeWordAtCursorPosition() {
    const wordToCapitalize = this.getWordPunctAtPosition();

    this.currentEditor.deleteText(wordToCapitalize.position.index, wordToCapitalize.position.length);
    this.currentEditor.insertText(wordToCapitalize.position.index, upperFirst(wordToCapitalize.value));
  }

  deleteWordAtCusorPosition() {
    const word = this.getWordPunctAtPosition();
    let spaceAdjustedIndex = word.position.index; // delete spaces preceding the word as well
    let lengthOffset = 0;
    while (!this.currentEditor.getText(spaceAdjustedIndex - 1, 1).trim() && spaceAdjustedIndex - 1 > 0) {
      spaceAdjustedIndex--;
      lengthOffset++;
    }

    this.currentEditor.deleteText(spaceAdjustedIndex, word.position.length + lengthOffset);
    const nextWord = this.getWordPunctAtPosition();
    this.setWordPunctSelection(nextWord);
  }

  deleteSentenceBeforePosition(position: RangeStatic = this.getSelection()) {
    const sentence: Sentence = this.getSentenceAtPosition(position);
    let endIndex = sentence.range.index + sentence.range.length;
    while (includes(VALID_SENTENCE_END, this.currentEditor.getText(++endIndex, 1))) {}
    this.currentEditor.deleteText(sentence.range.index, endIndex - sentence.range.index);
  }

  deleteRange(range: RangeStatic) {
    this.currentEditor.deleteText(range.index, range.length);
  }

  deleteLastAddedPhrase() {
    if (this.lastAddedPhrase) {
      this.deleteRange(this.lastAddedPhrase.range);
      this.lastAddedPhrase = null;
    }
  }

  async showSynonymPopover() {
    const event = new Event('HighlightedWord');
    const target = document.querySelector('[style*="background-color: pink;"]');
    this.thesaurusService.refreshSynonymList((target as any).innerText)
      .subscribe(async () => {
        Object.defineProperty(event, 'target', { writable: false, value: target });
        this.synonymPopover = await this.popoverController.create({
          component: SynonymPopoverComponent,
          event: event,
          translucent: true,
          showBackdrop: false,
          cssClass: 'editor-popover-identifier'
        });
        return await this.synonymPopover.present();
      });
  }

  async hideSynonymPopover() {
    if (this.synonymPopover) {
      return await this.synonymPopover.dismiss();
    }
  }

  async showDefinitionPopover() {
    const event = new Event('HighlightedWord');
    const target = document.querySelector('[style*="background-color: pink;"]');
    this.thesaurusService.refreshDefinitionsList((target as any).innerText)
      .subscribe(async () => {
        Object.defineProperty(event, 'target', { writable: false, value: target });
        this.definitionPopover = await this.popoverController.create({
          component: DefinitionPopoverComponent,
          event: event,
          translucent: true,
          showBackdrop: false,
          cssClass: 'editor-popover-identifier'
        });
        return await this.definitionPopover.present();
      });
  }

  async hideDefinitionPopover() {
    if (this.definitionPopover) {
      return await this.definitionPopover.dismiss();
    }
  }

  getPassiveVoice(script) {
    /**
     var fs = this;
     if (script.text.length > maxScanLen) {
        return[];
      }
     var contextElem = script.text.match(/\s(is|are|was|were|be|been|being)\s([a-z]{2,30})\b(\sby\b)?/gi);
     return contextElem ? contextElem.filter(function(segment) {
        var parts = segment.match(/([a-z]+)\b(\sby\b)?$/i);
        if (!parts) {
          return false;
        }
        var part = parts[1];
        var getPassiveVoices = part.match(/ed$/) || void 0 !== out.default[part.toLowerCase()];
        return getPassiveVoices;
      }).map(function(requestUrl) {
        return requestUrl.replace(/^\s/, "");
      }).map(function(lines, callback, details) {
        return fs.buildTreeFragment(self.default.tokens.passiveVoice, lines, fs.getPassiveIndex(script, lines, callback, details));
      }).map(function(opt) {
        return fs.addTrailingSpaceSetting(opt, script);
      }) : [];
     **/
  }

  getAdverbs() {
    /**
     var dojo = this;
     return s.items.filter(function(script) {
        return script.text.match(/ly$/);
      }).filter(function(target) {
        return void 0 === obj.default[target.text.toLowerCase()];
      }).map(function(node) {
        return dojo.buildTreeFragment(self.default.tokens.adverb, node.text, node.startIndex);
      }).map(function(tag) {
        return dojo.addTrailingSpaceSetting(tag, s);
      });
     **/
  }

  getItemDelimiter(dataAndEvents) {
    switch (dataAndEvents) {
      case 'root':
        return '[\n]+';
      case 'paragraph':
        return '[.?!]{1,2}["\u201d\'\\)]?(?:\\s|$)';
      case 'sentence':
        return '[^\\w\'-]';
      default:
        return '';
    }
  }

  getSubType(deepDataAndEvents) {
    switch (deepDataAndEvents) {
      case 'root':
        return 'paragraph';
      case 'paragraph':
        return 'sentence';
      case 'sentence':
        return 'word';
      default:
        return '';
    }
  }
}
