import {
  AfterViewInit,
  Component, ElementRef,
  EventEmitter, forwardRef,
  Input, OnChanges, OnDestroy,
  Output, SimpleChanges,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Delta } from 'quill';
import { QuillEditorService } from './quill-editor.service';
import { SettingsService } from 'src/app/services/settings.service';
declare var require: any;
const Quill = require('quill');

@Component({
  selector: 'quill-editor',
  template: `<div class="quill-editor" [ngClass]="recordingFontSize"></div>`,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => QuillEditorComponent),
    multi: true
  }],
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['quillEditor.component.scss']
})
export class QuillEditorComponent implements AfterViewInit, ControlValueAccessor, OnChanges, OnDestroy {

  quillEditor: any;
  editorElem: HTMLElement;
  content: any;
  defaultModules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],

      [{ header: 1 }, { header: 2 }],               // custom button values
      [{ list: 'ordered'}, { list: 'bullet' }],
      [{ script: 'sub'}, { script: 'super' }],      // superscript/subscript
      [{ indent: '-1'}, { indent: '+1' }],          // outdent/indent
      [{ direction: 'rtl' }],                         // text direction

      [{ size: ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ header: [1, 2, 3, 4, 5, 6, false] }],

      [{ color: new Array<any>() }, { background: new Array<any>() }],          // dropdown with defaults from theme
      [{ font: new Array<any>() }],
      [{ align: new Array<any>() }],

      ['clean'],                                         // remove formatting button

      ['link', 'image', 'video']                         // link and image, video
    ]
  };
  microphoneOnFontSize: number;

  @Input() options: Object;
  @Input() microphoneOn: boolean;


  @Output() blur: EventEmitter<any> = new EventEmitter();
  @Output() focus: EventEmitter<any> = new EventEmitter();
  @Output() ready: EventEmitter<any> = new EventEmitter();
  @Output() change: EventEmitter<any> = new EventEmitter();

  
  onModelChange: Function = () => {};
  onModelTouched: Function = () => {};

  constructor(
    private elementRef: ElementRef,
    private keyboard: Keyboard,
    private quillEditorService: QuillEditorService,
    private settingsService: SettingsService) { }

  ngAfterViewInit() {
    this.editorElem = this.elementRef.nativeElement.children[0];

    this.quillEditor = new Quill(this.editorElem, Object.assign({
      // modules: this.defaultModules,
      placeholder: 'Insert text here ...',
      readOnly: false,
      boundary: document.body,
      scrollingContainer: '#scrollable-editor',
    }, this.options || {}));

    
    this.quillEditorService.setCurrentEditor(this.quillEditor);

    // if (this.content) {
    //   this.quillEditor.pasteHTML(this.content);
    // }

    this.keyboard.onKeyboardShow().subscribe(data => {
      this.quillEditor.scrollIntoView(); // we use this because focus causes keyboard jitter
    });

    this.ready.emit(this.quillEditor);

    // mark model as touched if editor lost focus
    this.quillEditor.on('selection-change', (range: any) => {
      if (!range) {
        this.onModelTouched();
        this.blur.emit(this.quillEditor);
      } else {
        this.focus.emit(this.quillEditor);
      }
    });

    // update model if text changes
    this.quillEditor.on('text-change', (delta: any, oldDelta: any, source: any) => {
      let html = this.editorElem.children[0].innerHTML;
      const text = this.quillEditor.getText();

      if (html === '<p><br></p>') { html = null; }

      this.onModelChange({
        editor: this.quillEditor,
        html: html,
        text: text
      });

      this.change.emit({
        editor: this.quillEditor,
        html: html,
        text: text
      });
    });

    this.settingsService.state$.subscribe(settings => {
      this.microphoneOnFontSize = settings.fontSizeWhileRecording;
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.readOnly && this.quillEditor) {
      this.quillEditor.enable(!changes.readOnly.currentValue);
    }
  }

  ngOnDestroy() {
    this.quillEditor.blur();
    console.log('onDestroy');
  }

  writeValue(currentValue: Delta) {
    this.content = currentValue;

    if (this.quillEditor) {
      if (currentValue) {
        if (this.quillEditor.getContents()) {
          this.quillEditor.updateContents(currentValue);
        } else {
          this.quillEditor.setContents(currentValue);
        }
        return;
      }
      this.quillEditor.setText('');
    }
  }

  registerOnChange(fn: Function): void {
    this.onModelChange = fn;
  }

  registerOnTouched(fn: Function): void {
    this.onModelTouched = fn;
  }

  get recordingFontSize() {
    let fontSizeClass = 'quill-editor--font-size-';
    if (this.microphoneOn) {
      fontSizeClass += this.microphoneOnFontSize;
    } else {
      fontSizeClass += '1';
    }
    return fontSizeClass;
  }

}
